# doodleLib-ohos

#### 介绍
移植于github上涂鸦库，地址：https://github.com/1993hzw/Doodle

图片涂鸦，具有撤消、缩放、移动、添加文字，贴图等功能。还是一个功能强大，可自定义和可扩展的涂鸦框架、多功能画板


#### 效果图

![效果图](run.jpg)

#### 功能特性

- 画笔及形状

   通过该库可以在选择的图片上手绘、、橡皮擦、文字、贴图，仿制PS中的类似功能，可以在图片上添加箭头、矩形、圆、线段等形状，画笔的底色可以选择颜色，或者一张图片。
   
  
- 撤销/重做
  
   每一步的涂鸦操作都可以撤销
  
- 保存图片
   
   绘制结束后保存图片，可以将绘制的效果生成新的图片保存在媒体库，在相册中浏览
  
- 放缩、移动及旋转

  在涂鸦的过程中，可以自由地通过手势缩放、移动、旋转图片。可对涂鸦移动、旋转、缩放等

- Zoomer 放大器
  
  涂鸦过程中可以设置出现放大器
  
#### 软件架构
软件架构说明


#### 安装教程

1.  在工程的build.gradle文件中添加
    ```
    allprojects {
    	repositories {
    		...
    		maven { url 'https://jitpack.io' }
    	}
    }
    

2.  在model的build.gradle中添加
    
    ```
    dependencies {
        implementation 'com.gitee.talkwebyunchaung:doodleLib-ohos'
    }


#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
