package cn.talk.doodle.component;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.Slider;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.Line;
import ohos.agp.utils.Point;
import ohos.app.Context;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

/**
 * @author xiaoz
 * @version 1.0
 * @description TODO
 * @classname DoodleSeekBar
 * @program: doodleLib-ohos
 * @date 2021/6/16
 **/
public class DoodleSeekBar extends Component implements Component.DrawTask, Component.TouchEventListener {

    private static final Color LINE_COLOR = new Color(Color.getIntColor("#d0ced1"));

    private static final Color PROGRESS_COLOR = Color.WHITE;

    private int progress = 0;

    private int padding = 20;

    private Paint mPaint;

    private int lineHeight = 20;

    private ValueChangedListener valueChangedListener;

    public DoodleSeekBar(Context context) {
        this(context, null);
    }

    public DoodleSeekBar(Context context, AttrSet attrSet) {
        this(context, attrSet, "");
    }

    public DoodleSeekBar(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init();
    }

    public DoodleSeekBar(Context context, AttrSet attrSet, int resId) {
        super(context, attrSet, resId);
        init();
    }

    private void init() {
        setTouchEventListener(this);
        addDrawTask(this);
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setColor(LINE_COLOR);
        mPaint.setStyle(Paint.Style.FILL_STYLE);
        mPaint.setStrokeWidth(4);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        mPaint.setColor(LINE_COLOR);
        float xwidth = getWidth();
        float yheight = getHeight() / 2;
        mPaint.setStrokeWidth(20);
        Point startPoint = new Point(padding, yheight);
        Point endPoint = new Point(xwidth - padding, yheight);
        Line line = new Line(startPoint, endPoint);
        canvas.drawLine(line, mPaint);

        mPaint.setColor(PROGRESS_COLOR);
        mPaint.setStrokeWidth(4);
        float lineWidth = xwidth - padding * 2;
        float left = lineWidth * progress / 100;
        canvas.drawCircle(padding + left, yheight, 20, mPaint);
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        MmiPoint pointerPosition = touchEvent.getPointerPosition(0);
        boolean isHor = true;
        float mx = pointerPosition.getX();
        float barWidth = getWidth();
        progress = (int) (100 * mx / barWidth);
        progress = progress >= 0 ? progress : 0;
        progress = progress <= 100 ? progress : 100;
        setProgress(progress);
        return true;
    }


    public void setProgress(int progress) {
        this.progress = progress;
        if (valueChangedListener != null) {
            valueChangedListener.onProgressUpdated(this, progress);
        }
        invalidate();
    }

    public void setValueChangedListener(ValueChangedListener valueChangedListener) {
        this.valueChangedListener = valueChangedListener;
    }

    public interface ValueChangedListener {
        public void onProgressUpdated(DoodleSeekBar slider, int progress);
    }
}
