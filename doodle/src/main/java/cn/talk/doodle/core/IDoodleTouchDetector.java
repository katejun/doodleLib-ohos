package cn.talk.doodle.core;

import ohos.multimodalinput.event.TouchEvent;

public interface IDoodleTouchDetector {
    public boolean onTouchEvent(TouchEvent event);
}
