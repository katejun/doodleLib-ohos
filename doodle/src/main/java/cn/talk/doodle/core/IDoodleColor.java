package cn.talk.doodle.core;

import ohos.agp.render.Paint;

public interface IDoodleColor {

    /**
     * 配置画笔
     * @param doodleItem
     * @param paint
     */
    public void config(IDoodleItem doodleItem, Paint paint);

    /**
     * 深度拷贝
     * @return
     */
    public IDoodleColor copy();
}
