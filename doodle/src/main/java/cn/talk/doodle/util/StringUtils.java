package cn.talk.doodle.util;


import ohos.aafwk.ability.AbilitySlice;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.ResourceManager;
import ohos.global.resource.WrongTypeException;
import ohos.global.resource.Element;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.TimeZone;



public final class StringUtils {


    private StringUtils() {
    }

    /**
     * 是否为空
     *
     * @param content string
     * @return true if is null
     */
    public static boolean isEmpty(String content) {
        return content == null ||
                content.length() == 0 ||
                content.trim().length() == 0 ||
                "null".equalsIgnoreCase(content);
    }

    /**
     * 是否为空
     *
     * @param list
     * @return
     */
    public static boolean isEmpty(List list) {
        return null == list || list.size() == 0;
    }

    /**
     * 是否为空
     *
     * @param list
     * @return
     */
    public static boolean isEmpty(Object[] list) {
        return null == list || list.length == 0;
    }

    /**
     * 是否为空
     *
     * @param obj
     * @return
     */
    public static boolean isEmpty(Object obj) {
        return obj == null || isEmpty(obj.toString());
    }

    /**
     * 毫秒转换为时分秒
     * HH是24小时制的，hh是12小时
     * setTimeZone用于设置时区，如果不设置就会多出来8小时。
     * @param time
     * @return
     */
    public static String getTime(long time){
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
        formatter.setTimeZone(TimeZone.getTimeZone("GMT+00:00"));
        String hms = formatter.format(time);
        return hms;
    }

    /**
     * 根据resID获取对应的资源res
     *
     * @param resIDs
     * @return
     * @throws NotExistException
     * @throws WrongTypeException
     * @throws IOException
     */
    public static String[] getStringArray(AbilitySlice abilitySlice,int resIDs[]) throws NotExistException, WrongTypeException, IOException {
        if (null == resIDs && resIDs.length <= 0) {
            return null;
        }

        String[] strings = new String[resIDs.length];

        for (int i = 0; i < resIDs.length; i++) {
            ResourceManager resourceManager = abilitySlice.getResourceManager();
            String string = resourceManager.getElement(resIDs[i]).getString();
            strings[i] = string;
        }
        return strings;
    }

    public static String getString(Context context, int resourceId) {
        String resourceString = "";
        Element element = null;

        try {
            ResourceManager resourceManager = context.getResourceManager();
            element = resourceManager.getElement(resourceId);
            resourceString = element.getString();
        } catch (IOException var5) {
            var5.printStackTrace();
        } catch (NotExistException var6) {
            var6.printStackTrace();
        } catch (WrongTypeException var7) {
            var7.printStackTrace();
        }

        return resourceString;
    }

}
