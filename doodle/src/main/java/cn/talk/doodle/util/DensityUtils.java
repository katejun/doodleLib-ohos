package cn.talk.doodle.util;

import ohos.agp.components.AttrHelper;
import ohos.agp.utils.Point;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

public final class DensityUtils {
    public static int SCREEN_WIDTH_PIXELS;
    public static int SCREEN_HEIGHT_PIXELS;
    public static int SCREEN_REAL_WIDTH_PIXELS;
    public static int SCREEN_REAL_HEIGHT_PIXELS;
    public static int SCREEN_POINT_WIDTH_PIXELS;
    public static int SCREEN_POINT_HEIGHT_PIXELS;
    public static float SCREEN_DENSITY = 0.0F;
    public static float TEXT_SCALED_DENSITY = 0.0F;
    private static boolean mInitialed = false;

    private DensityUtils() {
    }

    public static void init(Context context) {
        if (!mInitialed && context != null) {
            mInitialed = true;
            Display display = getDisplay(context);
            DisplayAttributes dm = display.getRealAttributes();
            SCREEN_WIDTH_PIXELS = dm.width;
            SCREEN_HEIGHT_PIXELS = dm.height;
            SCREEN_DENSITY = dm.densityPixels;
            TEXT_SCALED_DENSITY = dm.scalDensity;
            DisplayAttributes dmr = display.getAttributes();
            SCREEN_REAL_WIDTH_PIXELS = dmr.width;
            SCREEN_REAL_HEIGHT_PIXELS = dmr.height;
            Point point = new Point();
            display.getSize(point);
            SCREEN_POINT_WIDTH_PIXELS = point.getPointXToInt();
            SCREEN_POINT_HEIGHT_PIXELS = point.getPointYToInt();
            print(dm);
        }
    }

    private static void print(DisplayAttributes attr) {
        StringBuilder sb = new StringBuilder();
        sb.append("屏幕参数：\n");
        sb.append("sw=");
        sb.append(attr.width);
        sb.append(", sh=");
        sb.append(attr.height);
        sb.append(", density(px,dpi)=(");
        sb.append(attr.densityPixels);
        sb.append(",");
        sb.append(attr.densityDpi);
        sb.append("), scaleDensity=");
        sb.append(attr.scalDensity);
        sb.append(",xDpi=");
        sb.append(attr.xDpi);
        sb.append(",yDpi=");
        sb.append(attr.yDpi);
        sb.append(", sd=");
        sb.append(SCREEN_DENSITY);
        sb.append(",vp2px=");
        sb.append(vp2px(10.0F));
        sb.append(",px2vp=");
        sb.append(px2vp(10.0F));
        System.out.println(sb.toString());
    }

    public static int vp2px(float vpValue) {
        return vp2px((Context)null, vpValue);
    }

    public static int vp2px(Context context, float vpValue) {
        if (SCREEN_DENSITY == 0.0F) {
            init(context);
        }

        float scale = SCREEN_DENSITY;
        return AttrHelper.vp2px(vpValue, scale);
    }

    public static int px2vp(float pxValue) {
        return px2vp((Context)null, pxValue);
    }

    public static int px2vp(Context context, float pxValue) {
        if (SCREEN_DENSITY == 0.0F) {
            init(context);
        }

        float scale = SCREEN_DENSITY;
        return Float.valueOf(pxValue / scale).intValue();
    }

    public static int fp2px(float fpValue) {
        return fp2px((Context)null, fpValue);
    }

    public static int fp2px(Context context, float fpValue) {
        if (TEXT_SCALED_DENSITY == 0.0F) {
            init(context);
        }

        float fontScale = TEXT_SCALED_DENSITY;
        return AttrHelper.fp2px(fpValue, fontScale);
    }

    private static Display getDisplay(Context ct) {
        if (ct == null) {
            throw new NullPointerException("init display object need context not be null.");
        } else {
            return (Display)DisplayManager.getInstance().getDefaultDisplay(ct).get();
        }
    }

    public static int[] getScreenSize(Context ct) {
        if (!mInitialed) {
            init(ct);
        }

        return new int[]{SCREEN_WIDTH_PIXELS, SCREEN_HEIGHT_PIXELS};
    }

    public static int[] getScreenRealSize(Context ct) {
        if (!mInitialed) {
            init(ct);
        }

        return new int[]{SCREEN_REAL_WIDTH_PIXELS, SCREEN_REAL_HEIGHT_PIXELS};
    }

    public static int getScreenWidth(Context context) {
        Display display = getDisplay(context);
        DisplayAttributes dm = display.getRealAttributes();
        return dm.width;
    }
}

