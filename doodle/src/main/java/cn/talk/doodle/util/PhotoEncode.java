package cn.talk.doodle.util;

import ohos.media.image.ImagePacker;
import ohos.media.image.PixelMap;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * @version 1.0.1
 * @description: 图像编码
 * @program: Gallery
 * @Author xiaozhijun
 * @Date 2020/12/22 14:43
 */
public class PhotoEncode {

    private static final String TAG = "PhotoEncode";

    //打包的参数类型
    ImagePacker.PackingOptions packingOptions;

    private PhotoEncode(ImagePacker.PackingOptions options) {
        this.packingOptions = options;
    }

    /**
     * 构建器
     */
    public static class Bulider {

        //打包参数
        private ImagePacker.PackingOptions packingOptions;

        public Bulider() {
            this.packingOptions = new ImagePacker.PackingOptions();
        }

        /**
         * 可根据ImagePacker.getSupportedFormat()获取所有支持的格式
         *
         * @param format 图像格式
         * @return
         */
        public Bulider format(String format) {
            //设置打包的图像格式  如设置为"image/jpeg"
            this.packingOptions.format = format;
            return this;
        }

        public Bulider quality(int quality) {
            //设置打包的图像质量，范围从0-100
            this.packingOptions.quality = quality;
            return this;
        }

        public PhotoEncode build() {
            return new PhotoEncode(this.packingOptions);
        }

    }

    /**
     * 图像编码 并保存为文件
     *
     * @param cachepath 设置缓存文件路径
     * @param pixelMap  图像数据
     * @return result 编码 成功/失败
     */
    public boolean encodePhoto(String cachepath, PixelMap pixelMap) {
        System.out.println("encodePhoto cachepath is " + cachepath);
        ImagePacker imagePacker = ImagePacker.create();
        File file = new File(cachepath);
        if (!file.getParentFile().exists()) {
            boolean isMkdirs = file.mkdirs();
            System.out.println("encodePhoto create cacheDire result is " + isMkdirs);
        }
        FileOutputStream outputStream = null;
        boolean result = false;
        try {
            outputStream = new FileOutputStream(cachepath);
            boolean initSuccess = imagePacker.initializePacking(outputStream, packingOptions);
            if (initSuccess) {
                boolean isAddSuccess = imagePacker.addImage(pixelMap);
                if (isAddSuccess) {
                    long dataSize = imagePacker.finalizePacking();
                    if (dataSize > 0) {
                        result = true;
                    }
                }
                outputStream.flush();
            }

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("encodePhoto exception is " + e.getMessage());
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (imagePacker != null) {
                imagePacker.release();
            }
        }
        return result;
    }

    /**
     * 图像编码 并保存为文件
     *
     * @param outputStream 缓存文件流
     * @param pixelMap  图像数据
     * @return result 编码 成功/失败
     */
    public boolean encodePhoto(OutputStream outputStream, PixelMap pixelMap) {
        if(outputStream == null || pixelMap == null){
            return false;
        }
        ImagePacker imagePacker = ImagePacker.create();
        boolean result = false;
        try {
            boolean initSuccess = imagePacker.initializePacking(outputStream, packingOptions);
            if (initSuccess) {
                boolean isAddSuccess = imagePacker.addImage(pixelMap);
                if (isAddSuccess) {
                    long dataSize = imagePacker.finalizePacking();
                    if (dataSize > 0) {
                        result = true;
                    }
                }
                outputStream.flush();
            }

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(TAG+ "encodePhoto exception is " + e.getMessage());
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            imagePacker.release();
        }
        return result;
    }

}
