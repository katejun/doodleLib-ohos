package cn.talk.doodle.util;

import java.io.File;
import java.io.IOException;

public class FileUtils {

    /**
     * 创建文件
     *
     * @param dir
     * @param fileName
     * @return
     */
    public static File createFile(String dir, String fileName) {
        File folderDir = new File(dir);
        if (!folderDir.exists()) {
            folderDir.mkdirs();
        }
        String filePath;
        if (dir.endsWith(File.separator)) {
            filePath = dir + fileName;
        } else {
            filePath = dir + File.separator + fileName;
        }
        File fileNew = new File(filePath);
        if (!fileNew.exists()) {
            try {
                fileNew.createNewFile();
            } catch (IOException e) {
                return null;
            }
        }
        return fileNew;
    }
}
