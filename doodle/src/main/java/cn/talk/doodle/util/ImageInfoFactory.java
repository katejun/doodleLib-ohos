package cn.talk.doodle.util;

import java.io.*;

/**
 * @version 1.0.1
 * @description: 大图信息工厂类 提供获取图片宽高
 * @program: Gallery
 * @Author xiaozhijun
 * @Date 2021/1/4 16:46
 */
public class ImageInfoFactory {

    //图片高度
    private int height;

    //图片宽度
    private int width;

    //图片类型
    private String mimeType;


    /**
     * @param file 解码图片文件宽高
     * @return boolean 成功/失败
     * @throws IOException
     */
    public boolean decodeStream(File file) throws IOException {
        boolean result = false;
        InputStream is = new FileInputStream(file);
        try {
            result = decodeStream(is);
        } finally {
            is.close();
        }
        return result;
    }


    /**
     * @param bytes 解码图片数据宽高
     * @return boolean 成功/失败
     * @throws IOException
     */
    public boolean decodeStream(byte[] bytes) throws IOException {
        boolean result = false;
        InputStream is = new ByteArrayInputStream(bytes);
        try {
            result = decodeStream(is);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            is.close();
        }
        return result;
    }

    /**
     * @param is 解码图片流宽高
     * @return boolean 成功/失败
     * @throws IOException
     */
    public boolean decodeStream(InputStream is) throws IOException {
        boolean result = false;
        int c1 = is.read();
        int c2 = is.read();
        int c3 = is.read();

        mimeType = null;
        width = height = -1;

        if (c1 == 'G' && c2 == 'I' && c3 == 'F') { // GIF
            is.skip(3);
            width = readInt(is, 2, false);
            height = readInt(is, 2, false);
            mimeType = "image/gif";
            result = true;
        } else if (c1 == 0xFF && c2 == 0xD8) { // JPG
            while (c3 == 255) {
                int marker = is.read();
                int len = readInt(is, 2, true);
                if (marker == 192 || marker == 193 || marker == 194) {
                    is.skip(1);
                    height = readInt(is, 2, true);
                    width = readInt(is, 2, true);
                    mimeType = "image/jpeg";
                    break;
                }
                is.skip(len - 2);
                c3 = is.read();
            }
            result = true;
        } else if (c1 == 137 && c2 == 80 && c3 == 78) { // PNG
            is.skip(15);
            width = readInt(is, 2, true);
            is.skip(2);
            height = readInt(is, 2, true);
            mimeType = "image/png";
            result = true;
        } else if (c1 == 66 && c2 == 77) { // BMP
            is.skip(15);
            width = readInt(is, 2, false);
            is.skip(2);
            height = readInt(is, 2, false);
            mimeType = "image/bmp";
            result = true;
        } else {
            int c4 = is.read();
            if ((c1 == 'M' && c2 == 'M' && c3 == 0 && c4 == 42)
                    || (c1 == 'I' && c2 == 'I' && c3 == 42 && c4 == 0)) { //TIFF
                boolean bigEndian = c1 == 'M';
                int ifd = 0;
                int entries;
                ifd = readInt(is, 4, bigEndian);
                is.skip(ifd - 8);
                entries = readInt(is, 2, bigEndian);
                for (int i = 1; i <= entries; i++) {
                    int tag = readInt(is, 2, bigEndian);
                    int fieldType = readInt(is, 2, bigEndian);
                    int valOffset;
                    if ((fieldType == 3 || fieldType == 8)) {
                        valOffset = readInt(is, 2, bigEndian);
                        is.skip(2);
                    } else {
                        valOffset = readInt(is, 4, bigEndian);
                    }
                    if (tag == 256) {
                        width = valOffset;
                    } else if (tag == 257) {
                        height = valOffset;
                    }
                    if (width != -1 && height != -1) {
                        mimeType = "image/tiff";
                        break;
                    }
                }
            }
            result = true;
        }
        if (mimeType == null) {
            throw new IOException("Unsupported image type");
        }
        return result;
    }

    /**
     * 从流中读取一个Int数据
     *
     * @param is
     * @param noOfBytes
     * @param bigEndian
     * @return
     * @throws IOException
     */
    private int readInt(InputStream is, int noOfBytes, boolean bigEndian) throws IOException {
        int ret = 0;
        int sv = bigEndian ? ((noOfBytes - 1) * 8) : 0;
        int cnt = bigEndian ? -8 : 8;
        for (int i = 0; i < noOfBytes; i++) {
            ret |= is.read() << sv;
            sv += cnt;
        }
        return ret;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    @Override
    public String toString() {
        return "MIME Type : " + mimeType + "\t Width : " + width
                + "\t Height : " + height;
    }


}
