package cn.talk.doodle.ability.base.gesture;


import ohos.agp.components.Component;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.multimodalinput.event.TouchEvent;

import java.util.logging.Handler;

public class ScaleGestureDetectorApi27 {
    private static final String TAG = "ScaleGestureDetectorApi27";
    private final Context mContext;
    private final ScaleGestureDetectorApi27.OnScaleGestureListener mListener;
    private float mFocusX;
    private float mFocusY;
    private boolean mQuickScaleEnabled;
    private boolean mStylusScaleEnabled;
    private float mCurrSpan;
    private float mPrevSpan;
    private float mInitialSpan;
    private float mCurrSpanX;
    private float mCurrSpanY;
    private float mPrevSpanX;
    private float mPrevSpanY;
    private long mCurrTime;
    private long mPrevTime;
    private boolean mInProgress;
    private int mSpanSlop;
    private int mMinSpan;
    private final EventHandler mHandler;
    private float mAnchoredScaleStartX;
    private float mAnchoredScaleStartY;
    private int mAnchoredScaleMode;
    private static final long TOUCH_STABILIZE_TIME = 128L;
    private static final float SCALE_FACTOR = 0.5F;
    private static final int ANCHORED_SCALE_MODE_NONE = 0;
    private static final int ANCHORED_SCALE_MODE_DOUBLE_TAP = 1;
    private static final int ANCHORED_SCALE_MODE_STYLUS = 2;
    private GestureDetector mGestureDetector;
    private boolean mEventBeforeOrAboveStartingGestureEvent;

    public ScaleGestureDetectorApi27(Context context, ScaleGestureDetectorApi27.OnScaleGestureListener listener) {
        this(context, listener, (EventHandler)null);
    }

    public ScaleGestureDetectorApi27(Context context, ScaleGestureDetectorApi27.OnScaleGestureListener listener, EventHandler handler) {
        this.mAnchoredScaleMode = 0;
        this.mContext = context;
        this.mListener = listener;
        this.mSpanSlop = 8 * 2;
        this.mMinSpan = 27;
        this.mHandler = handler;
        this.setStylusScaleEnabled(true);

    }

    public boolean onTouchEvent(TouchEvent event) {
        this.mCurrTime = event.getStartTime();
        int action = event.getAction();
        if (this.mQuickScaleEnabled) {
            this.mGestureDetector.onTouchEvent(event);
        }

        int count = event.getPointerCount();
        boolean isStylusButtonDown = (event.getPhase() & 1) != 0;
        boolean anchoredScaleCancelled = this.mAnchoredScaleMode == 2 && !isStylusButtonDown;
        boolean streamComplete = action == 1 || action == 3 || anchoredScaleCancelled;
        if (action == 0 || streamComplete) {
            if (this.mInProgress) {
                this.mListener.onScaleEnd(this);
                this.mInProgress = false;
                this.mInitialSpan = 0.0F;
                this.mAnchoredScaleMode = 0;
            } else if (this.inAnchoredScaleMode() && streamComplete) {
                this.mInProgress = false;
                this.mInitialSpan = 0.0F;
                this.mAnchoredScaleMode = 0;
            }

            if (streamComplete) {
                return true;
            }
        }

        if (!this.mInProgress && this.mStylusScaleEnabled && !this.inAnchoredScaleMode() && !streamComplete && isStylusButtonDown) {
            this.mAnchoredScaleStartX = event.getPointerPosition(0).getX();
            this.mAnchoredScaleStartY = event.getPointerPosition(0).getY();
            this.mAnchoredScaleMode = 2;
            this.mInitialSpan = 0.0F;
        }

        boolean configChanged = action == 0 || action == 6 || action == 5 || anchoredScaleCancelled;
        boolean pointerUp = action == 6;
        int skipIndex = pointerUp ? event.getIndex() : -1;
        float sumX = 0.0F;
        float sumY = 0.0F;
        int div = pointerUp ? count - 1 : count;
        float focusX;
        float focusY;
        if (this.inAnchoredScaleMode()) {
            focusX = this.mAnchoredScaleStartX;
            focusY = this.mAnchoredScaleStartY;
            if (event.getPointerPosition(event.getPointerCount() - 1).getX() < focusY) {
                this.mEventBeforeOrAboveStartingGestureEvent = true;
            } else {
                this.mEventBeforeOrAboveStartingGestureEvent = false;
            }
        } else {
            for(int i = 0; i < count; ++i) {
                if (skipIndex != i) {
                    sumX += event.getPointerPosition(i).getX();
                    sumY += event.getPointerPosition(i).getY();
                }
            }

            focusX = sumX / (float)div;
            focusY = sumY / (float)div;
        }

        float devSumX = 0.0F;
        float devSumY = 0.0F;

        for(int i = 0; i < count; ++i) {
            if (skipIndex != i) {
                devSumX += Math.abs(event.getPointerPosition(i).getX() - focusX);
                devSumY += Math.abs(event.getPointerPosition(i).getY() - focusY);
            }
        }

        float devX = devSumX / (float)div;
        float devY = devSumY / (float)div;
        float spanX = devX * 2.0F;
        float spanY = devY * 2.0F;
        float span;
        if (this.inAnchoredScaleMode()) {
            span = spanY;
        } else {
            span = (float)Math.hypot((double)spanX, (double)spanY);
        }

        boolean wasInProgress = this.mInProgress;
        this.mFocusX = focusX;
        this.mFocusY = focusY;
        if (!this.inAnchoredScaleMode() && this.mInProgress && (span < (float)this.mMinSpan || configChanged)) {
            this.mListener.onScaleEnd(this);
            this.mInProgress = false;
            this.mInitialSpan = span;
        }

        if (configChanged) {
            this.mPrevSpanX = this.mCurrSpanX = spanX;
            this.mPrevSpanY = this.mCurrSpanY = spanY;
            this.mInitialSpan = this.mPrevSpan = this.mCurrSpan = span;
        }

        int minSpan = this.inAnchoredScaleMode() ? this.mSpanSlop : this.mMinSpan;
        if (!this.mInProgress && span >= (float)minSpan && (wasInProgress || Math.abs(span - this.mInitialSpan) > (float)this.mSpanSlop)) {
            this.mPrevSpanX = this.mCurrSpanX = spanX;
            this.mPrevSpanY = this.mCurrSpanY = spanY;
            this.mPrevSpan = this.mCurrSpan = span;
            this.mPrevTime = this.mCurrTime;
            this.mInProgress = this.mListener.onScaleBegin(this);
        }

        if (action == 2) {
            this.mCurrSpanX = spanX;
            this.mCurrSpanY = spanY;
            this.mCurrSpan = span;
            boolean updatePrev = true;
            if (this.mInProgress) {
                updatePrev = this.mListener.onScale(this);
            }

            if (updatePrev) {
                this.mPrevSpanX = this.mCurrSpanX;
                this.mPrevSpanY = this.mCurrSpanY;
                this.mPrevSpan = this.mCurrSpan;
                this.mPrevTime = this.mCurrTime;
            }
        }

        return true;
    }

    private boolean inAnchoredScaleMode() {
        return this.mAnchoredScaleMode != 0;
    }

    public void setQuickScaleEnabled(boolean scales) {
        this.mQuickScaleEnabled = scales;
        if (this.mQuickScaleEnabled && this.mGestureDetector == null) {
            GestureDetector.SimpleOnGestureListener gestureListener = new GestureDetector.SimpleOnGestureListener() {
                public boolean onDoubleTap(TouchEvent e) {
                    ScaleGestureDetectorApi27.this.mAnchoredScaleStartX = e.getPointerPosition(e.getPointerCount()-1).getX();
                    ScaleGestureDetectorApi27.this.mAnchoredScaleStartY = e.getPointerPosition(e.getPointerCount() -1).getY();
                    ScaleGestureDetectorApi27.this.mAnchoredScaleMode = 1;
                    return true;
                }
            };
            this.mGestureDetector = new GestureDetector(this.mContext, gestureListener, this.mHandler);
        }

    }

    public boolean isQuickScaleEnabled() {
        return this.mQuickScaleEnabled;
    }

    public void setStylusScaleEnabled(boolean scales) {
        this.mStylusScaleEnabled = scales;
    }

    public boolean isStylusScaleEnabled() {
        return this.mStylusScaleEnabled;
    }

    public boolean isInProgress() {
        return this.mInProgress;
    }

    public float getFocusX() {
        return this.mFocusX;
    }

    public float getFocusY() {
        return this.mFocusY;
    }

    public float getCurrentSpan() {
        return this.mCurrSpan;
    }

    public float getCurrentSpanX() {
        return this.mCurrSpanX;
    }

    public float getCurrentSpanY() {
        return this.mCurrSpanY;
    }

    public float getPreviousSpan() {
        return this.mPrevSpan;
    }

    public float getPreviousSpanX() {
        return this.mPrevSpanX;
    }

    public float getPreviousSpanY() {
        return this.mPrevSpanY;
    }

    public float getScaleFactor() {
        if (!this.inAnchoredScaleMode()) {
            return this.mPrevSpan > 0.0F ? this.mCurrSpan / this.mPrevSpan : 1.0F;
        } else {
            boolean scaleUp = this.mEventBeforeOrAboveStartingGestureEvent && this.mCurrSpan < this.mPrevSpan || !this.mEventBeforeOrAboveStartingGestureEvent && this.mCurrSpan > this.mPrevSpan;
            float spanDiff = Math.abs(1.0F - this.mCurrSpan / this.mPrevSpan) * 0.5F;
            return this.mPrevSpan <= 0.0F ? 1.0F : (scaleUp ? 1.0F + spanDiff : 1.0F - spanDiff);
        }
    }

    public long getTimeDelta() {
        return this.mCurrTime - this.mPrevTime;
    }

    public long getEventTime() {
        return this.mCurrTime;
    }

    public void setMinSpan(int minSpan) {
        this.mMinSpan = minSpan;
    }

    public void setSpanSlop(int spanSlop) {
        this.mSpanSlop = spanSlop;
    }

    public int getMinSpan() {
        return this.mMinSpan;
    }

    public int getSpanSlop() {
        return this.mSpanSlop;
    }

    public static class SimpleOnScaleGestureListener implements OnScaleGestureListener {
        public SimpleOnScaleGestureListener() {
        }
/*
        public boolean onScale(ScaleGestureDetector detector) {
            return false;
        }

        public boolean onScaleBegin(ScaleGestureDetector detector) {
            return true;
        }

        public void onScaleEnd(ScaleGestureDetector detector) {
        }*/

        @Override
        public boolean onScale(ScaleGestureDetectorApi27 var1) {
            return false;
        }

        @Override
        public boolean onScaleBegin(ScaleGestureDetectorApi27 var1) {
            return true;
        }

        @Override
        public void onScaleEnd(ScaleGestureDetectorApi27 var1) {

        }
    }

    public interface OnScaleGestureListener {
        boolean onScale(ScaleGestureDetectorApi27 var1);

        boolean onScaleBegin(ScaleGestureDetectorApi27 var1);

        void onScaleEnd(ScaleGestureDetectorApi27 var1);
    }
}
