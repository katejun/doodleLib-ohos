package cn.talk.doodle.ability.base;

import cn.talk.doodle.core.IDoodle;
import ohos.agp.render.Canvas;
import ohos.agp.render.PixelMapHolder;
import ohos.agp.utils.Rect;
import ohos.agp.utils.RectFloat;
import ohos.media.image.PixelMap;

/**
 * 图片item
 * Created by huangziwei on 2017/3/16.
 */

public class DoodleBitmap extends DoodleRotatableItemBase {

    private PixelMap mBitmap;
    private Rect mRect = new Rect();
    private Rect mSrcRect = new Rect();
    private Rect mDstRect = new Rect();

    public DoodleBitmap(IDoodle doodle, PixelMap bitmap, float size, float x, float y) {
        super(doodle, -doodle.getDoodleRotation(), x, y); // 设置item旋转角度，使其在当前状态下显示为“无旋转”效果
        setPen(DoodlePen.BITMAP);
        setPivotX(x);
        setPivotY(y);
        this.mBitmap = bitmap;
        setSize(size);
        setLocation(x, y);
    }

    public void setBitmap(PixelMap bitmap) {
        mBitmap = bitmap;
        resetBounds(mRect);
        setPivotX(getLocation().getPointX() + mRect.getWidth() / 2);
        setPivotY(getLocation().getPointY() + mRect.getHeight() / 2);
        resetBoundsScaled(getBounds());

        refresh();
    }

    public PixelMap getBitmap() {
        return mBitmap;
    }

    @Override
    public void resetBounds(Rect rect) {
        if (mBitmap == null) {
            return;
        }
        float size = getSize();
        rect.set(0, 0, (int) size, (int) (size * mBitmap.getImageInfo().size.height) / mBitmap.getImageInfo().size.width);

        mSrcRect.set(0, 0, mBitmap.getImageInfo().size.width, mBitmap.getImageInfo().size.height);
        mDstRect.set(0, 0, (int) size, (int) (size * mBitmap.getImageInfo().size.height) / mBitmap.getImageInfo().size.width);
    }

    @Override
    public void doDraw(Canvas canvas) {
        canvas.drawPixelMapHolderRect(new PixelMapHolder(mBitmap), new RectFloat(mSrcRect),new RectFloat(mDstRect) , null);
    }

}


