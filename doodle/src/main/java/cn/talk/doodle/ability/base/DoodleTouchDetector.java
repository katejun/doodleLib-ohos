package cn.talk.doodle.ability.base;


import cn.talk.doodle.ability.base.gesture.TouchGestureDetector;
import cn.talk.doodle.core.IDoodleTouchDetector;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;

public class DoodleTouchDetector extends TouchGestureDetector implements IDoodleTouchDetector {

    private OnTouchGestureListener onTouchGestureListener;
    public DoodleTouchDetector(Context context, OnTouchGestureListener listener) {
        super(context, listener);
        // 下面两行绘画场景下应该设置间距为大于等于1，否则设为0双指缩放后抬起其中一个手指仍然可以移动
     /*   this.setScaleSpanSlop(1); // 手势前识别为缩放手势的双指滑动最小距离值
        this.setScaleMinSpan(1); // 缩放过程中识别为缩放手势的双指最小距离值

        this.setIsLongpressEnabled(false);
        this.setIsScrollAfterScaled(false);*/
        onTouchGestureListener = listener;
    }

    @Override
    public boolean onTouchEvent(TouchEvent touchEvent) {
        System.out.println("event.getAction()" +touchEvent.getAction());
        if (onTouchGestureListener != null){
            switch (touchEvent.getAction()) {
                case TouchEvent.PRIMARY_POINT_DOWN:

                    onTouchGestureListener.onScrollBegin(touchEvent);
                    break;
                case TouchEvent.POINT_MOVE:
                    // todo 需要检测长按状态
                   // onTouchGestureListener.onScroll(null, touchEvent, 0, 0);
                    onTouchGestureListener.onScrolling(touchEvent);
                    break;
                case TouchEvent.PRIMARY_POINT_UP:
                    onTouchGestureListener.onScrollEnd(touchEvent);
                    break;
            }

        }
        return true;
    }
}
