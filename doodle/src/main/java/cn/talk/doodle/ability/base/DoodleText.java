package cn.talk.doodle.ability.base;

import cn.talk.doodle.core.IDoodle;
import cn.talk.doodle.core.IDoodleColor;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.Rect;

/**
 * 文字item
 * Created by huangziwei on 2017/3/16.
 */

public class DoodleText extends DoodleRotatableItemBase {

    private Rect mRect = new Rect();
    private final Paint mPaint = new Paint();
    private String mText;

    public DoodleText(IDoodle doodle, String text, float size, IDoodleColor color, float x, float y) {
        super(doodle, -doodle.getDoodleRotation(), x, y);
        setPen(DoodlePen.TEXT);
        mText = text;
        setSize(size);
        setColor(color);
        setLocation(x, y);
    }

    public String getText() {
        return mText;
    }

    public void setText(String text) {
        mText = text;
        resetBounds(mRect);
        setPivotX(getLocation().getPointX() + mRect.getWidth() / 2);
        setPivotY(getLocation().getPointY() + mRect.getHeight() / 2);
        resetBoundsScaled(getBounds());

        refresh();
    }

    @Override
    public void resetBounds(Rect rect) {
        /*if (TextUtils.isEmpty(mText)) {
            return;
        }*/
        int szie = (int) getSize();
        mPaint.setTextSize(szie );
        mPaint.setStyle(Paint.Style.FILL_STYLE);
        mPaint.getTextBounds(mText);
        rect.offset(0, rect.getHeight());
    }

    @Override
    public void doDraw(Canvas canvas) {
        getColor().config(this, mPaint);
        int szie = (int) getSize();
        mPaint.setTextSize(szie);
        mPaint.setStyle(Paint.Style.FILL_STYLE);
        canvas.save();
        canvas.translate(0, getBounds().getHeight() / getScale());
        canvas.drawText( mPaint , mText ,0, 0);
        canvas.restore();
    }

}


