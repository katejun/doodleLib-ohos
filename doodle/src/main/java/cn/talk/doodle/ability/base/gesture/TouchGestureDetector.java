package cn.talk.doodle.ability.base.gesture;

import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;

public class TouchGestureDetector {

    public TouchGestureDetector(Context context, OnTouchGestureListener onTouchGestureListener) {
    }

    public TouchGestureDetector() {
    }

    public abstract static class OnTouchGestureListener implements TouchGestureDetector.IOnTouchGestureListener {
        public OnTouchGestureListener() {
        }

        public boolean onDown(TouchEvent e) {
            return false;
        }

        public void onUpOrCancel(TouchEvent e) {
        }

        public boolean onFling(TouchEvent e1, TouchEvent e2, float velocityX, float velocityY) {
            return false;
        }

        public void onLongPress(TouchEvent e) {
        }

        public void onScrollBegin(TouchEvent e) {
        }

        public void onScrollEnd(TouchEvent e) {
        }

        public boolean onScroll(TouchEvent e1, TouchEvent e2, float distanceX, float distanceY) {
            return false;
        }

        public void onShowPress(TouchEvent e) {
        }

        public boolean onSingleTapUp(TouchEvent e) {
            return false;
        }

        public boolean onDoubleTap(TouchEvent e) {
            return false;
        }

        public boolean onDoubleTapEvent(TouchEvent e) {
            return false;
        }

        @Override
        public boolean onSingleTapConfirmed(TouchEvent e) {
            return false;
        }

        public boolean onScale(ScaleGestureDetectorApi27 detector) {
            return false;
        }

        public boolean onScaleBegin(ScaleGestureDetectorApi27 detector) {
            return false;
        }

        public void onScaleEnd(ScaleGestureDetectorApi27 detector) {
        }
    }

    public interface IOnTouchGestureListener extends GestureDetector.OnGestureListener, GestureDetector.OnDoubleTapListener, ScaleGestureDetectorApi27.OnScaleGestureListener {
        void onUpOrCancel(TouchEvent var1);

        void onScrollBegin(TouchEvent var1);

        void onScrollEnd(TouchEvent var1);

        void onScrolling(TouchEvent var1);
    }


}
