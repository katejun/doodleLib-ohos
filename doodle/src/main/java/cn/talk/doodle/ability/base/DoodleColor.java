package cn.talk.doodle.ability.base;


import cn.talk.doodle.core.IDoodleColor;
import cn.talk.doodle.core.IDoodleItem;
import ohos.agp.render.Paint;
import ohos.agp.render.PixelMapHolder;
import ohos.agp.render.PixelMapShader;
import ohos.agp.utils.Color;
import ohos.agp.utils.Matrix;
import ohos.media.image.PixelMap;
import ohos.agp.render.Shader;

/**
 * 涂鸦画笔颜色，用于手绘
 */
public class DoodleColor implements IDoodleColor {

    public enum Type {
        COLOR, // 颜色值
        BITMAP // 图片
    }

    private int mColor;
    private PixelMap mBitmap;
    private Type mType;
    private Matrix mMatrix;

    private int mLevel = 1;

    // bitmap相关
    private Shader.TileMode mTileX = Shader.TileMode.MIRROR_TILEMODE;
    private Shader.TileMode mTileY = Shader.TileMode.MIRROR_TILEMODE;  // 镜像

    public DoodleColor(int color) {
        mType = Type.COLOR;
        mColor = color;
    }

    public DoodleColor(PixelMap bitmap) {
        this(bitmap, null);
    }

    public DoodleColor(PixelMap bitmap, Matrix matrix) {
        this(bitmap, matrix, Shader.TileMode.MIRROR_TILEMODE, Shader.TileMode.MIRROR_TILEMODE);
    }

    public DoodleColor(PixelMap bitmap, Matrix matrix, Shader.TileMode tileX, Shader.TileMode tileY) {
        mType = Type.BITMAP;
        mMatrix = matrix;
        mBitmap = bitmap;
        mTileX = tileX;
        mTileY = tileY;
    }

    @Override
    public void config(IDoodleItem item, Paint paint) {
        DoodleItemBase doodleItem = (DoodleItemBase) item;
        if (mType == Type.COLOR) {
            paint.setColor(new Color(mColor));
            paint.setShader(null, null);
        } else if (mType == Type.BITMAP) {
            PixelMapShader shader = new PixelMapShader(new PixelMapHolder(mBitmap), mTileX, mTileY);
           // shader.setLocalMatrix(mMatrix);
            paint.setShader(shader, Paint.ShaderType.PIXELMAP_SHADER);
        }
    }

    public void setColor(int color) {
        mType = Type.COLOR;
        mColor = color;
    }

    public void setColor(PixelMap bitmap) {
        mType = Type.BITMAP;
        mBitmap = bitmap;
    }

    public void setColor(PixelMap bitmap, Matrix matrix) {
        mType = Type.BITMAP;
        mMatrix = matrix;
        mBitmap = bitmap;
    }

    public void setColor(PixelMap bitmap, Matrix matrix, Shader.TileMode tileX, Shader.TileMode tileY) {
        mType = Type.BITMAP;
        mBitmap = bitmap;
        mMatrix = matrix;
        mTileX = tileX;
        mTileY = tileY;
    }

    public void setMatrix(Matrix matrix) {
        mMatrix = matrix;
    }

    public Matrix getMatrix() {
        return mMatrix;
    }

    public int getColor() {
        return mColor;
    }

    public PixelMap getBitmap() {
        return mBitmap;
    }

    public Type getType() {
        return mType;
    }

    @Override
    public IDoodleColor copy() {
        DoodleColor color = null;
        if (mType == Type.COLOR) {
            color = new DoodleColor(mColor);
        } else {
            color = new DoodleColor(mBitmap);
        }
        color.mTileX = mTileX;
        color.mTileY = mTileY;
        color.mMatrix = new Matrix(mMatrix);
        color.mLevel = mLevel;
        return color;
    }

    public void setLevel(int level) {
        mLevel = level;
    }

    public int getLevel() {
        return mLevel;
    }
}


