package cn.talk.doodle.ability;

import cn.talk.doodle.ability.slice.DoodleAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class DoodleAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(DoodleAbilitySlice.class.getName());
    }

}
