package cn.talk.doodle.ability.base;

import cn.talk.doodle.core.IDoodle;
import cn.talk.doodle.core.IDoodleItem;
import cn.talk.doodle.core.IDoodleShape;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;

/**
 * 常用图形
 */
public enum DoodleShape implements IDoodleShape {
    HAND_WRITE, // 手绘
    ARROW, // 箭头
    LINE, // 直线
    FILL_CIRCLE, // 实心圆
    HOLLOW_CIRCLE, // 空心圆
    FILL_RECT, // 实心矩形
    HOLLOW_RECT; // 空心矩形


    @Override
    public void config(IDoodleItem doodleItem, Paint paint) {
        if (doodleItem.getShape() == DoodleShape.ARROW || doodleItem.getShape() == DoodleShape.FILL_CIRCLE || doodleItem.getShape() == DoodleShape.FILL_RECT) {
            paint.setStyle(Paint.Style.FILL_STYLE);
        } else {
            paint.setStyle(Paint.Style.STROKE_STYLE);
        }
    }

    @Override
    public IDoodleShape copy() {
        return this;
    }

    @Override
    public void drawHelpers(Canvas canvas, IDoodle doodle) {

    }
}
