package cn.talk.doodle.ability.base;


import cn.talk.doodle.core.IDoodle;
import ohos.aafwk.ability.Ability;
import ohos.agp.utils.Color;
import ohos.utils.Parcel;
import ohos.utils.Sequenceable;

/**
 * 涂鸦参数
 */
public class DoodleParams implements Sequenceable {


    /**
     * 图片资源路径
     */
    public String mImageUriStr;
    /**
     * 图片路径
     */
    public String mImagePath;
    /**
     * 　保存路径，如果为null，则图片保存在根目录下/DCIM/Doodle/
     */
    public String mSavePath;
    /**
     * 　保存路径是否为目录，如果为目录，则在该目录生成由时间戳组成的图片名称
     */
    public boolean mSavePathIsDir;

    /**
     * 触摸时，图片区域外是否绘制涂鸦轨迹
     */
    public boolean mIsDrawableOutside;

    /**
     * 涂鸦时（手指按下）隐藏设置面板的延长时间(ms)，当小于等于0时则为不尝试隐藏面板（即保持面板当前状态不变）;当大于0时表示需要触摸屏幕超过一定时间后才隐藏
     * 或者手指抬起时展示面板的延长时间(ms)，或者表示需要离开屏幕超过一定时间后才展示
     * 默认为200ms
     */
    public long mChangePanelVisibilityDelay = 200; //ms

    /**
     * 设置放大镜的倍数，当小于等于0时表示不使用放大器功能
     * 放大器只有在设置面板被隐藏的时候才会出现
     * 默认为2.5倍
     */
    public float mZoomerScale = 2.5f;

    /**
     * 是否全屏显示，即是否隐藏状态栏
     * 默认为false，表示状态栏继承应用样式
     */
    public boolean mIsFullScreen = false;

    /**
     * 初始化的画笔大小,单位为像素
     */
    public float mPaintPixelSize = -1;

    /**
     * 初始化的画笔大小,单位为涂鸦坐标系中的单位大小，该单位参考dp，独立于图片
     * mPaintUnitSize值优先于mPaintPixelSize
     */
    public float mPaintUnitSize = -1;

    /**
     * 画布的最小/最大缩放倍数
     */
    public float mMinScale = DoodleView.MIN_SCALE;
    public float mMaxScale = DoodleView.MAX_SCALE;

    /**
     * 初始的画笔颜色
     */
    public int mPaintColor = Color.RED.getValue();

    /**
     * 是否支持缩放item
     */
    public boolean mSupportScaleItem = true;

    /**
     *
     * 是否优化绘制，开启后涂鸦会及时绘制在图片上，以此优化绘制速度和性能.
     *
     * {@link DoodleView#}
     */
    public boolean mOptimizeDrawing = true;

    public static final Producer<DoodleParams> PRODUCER  = new Producer<DoodleParams>() {
        @Override
        public DoodleParams createFromParcel(Parcel in) {
            DoodleParams params = new DoodleParams();
            params.unmarshalling(in);
            return params;
        }

    };


    private static DialogInterceptor sDialogInterceptor;

    /**
     * 设置涂鸦中对话框的拦截器，如点击返回按钮（或返回键）弹出保存对话框，可以进行拦截，弹出自定义的对话框
     * 切记：需要自行处理内存泄漏的问题！！！
     */
    public static void setDialogInterceptor(DialogInterceptor interceptor) {
        sDialogInterceptor = interceptor;
    }

    public static DialogInterceptor getDialogInterceptor() {
        return sDialogInterceptor;
    }

    @Override
    public boolean marshalling(Parcel out) {
        return out.writeString(mImageUriStr) && out.writeString(mImagePath) &&
                out.writeString(mSavePath) && out.writeBoolean(mSavePathIsDir) &&
                out.writeBoolean(mIsDrawableOutside) && out.writeLong(mChangePanelVisibilityDelay) &&
                out.writeFloat(mZoomerScale) && out.writeBoolean(mIsFullScreen) &&
                out.writeFloat(mPaintPixelSize) && out.writeFloat(mPaintUnitSize) &&
                out.writeFloat(mMinScale) && out.writeFloat(mMaxScale) &&
                out.writeInt(mPaintColor) && out.writeBoolean(mSupportScaleItem)
                && out.writeBoolean(mOptimizeDrawing);
    }

    @Override
    public boolean unmarshalling(Parcel in) {
        this.mImageUriStr = in.readString();
        this.mImagePath = in.readString();
        this.mSavePath = in.readString();
        this.mSavePathIsDir = in.readInt() == 1;
        this.mIsDrawableOutside = in.readInt() == 1;
        this.mChangePanelVisibilityDelay = in.readLong();
        this.mZoomerScale = in.readFloat();
        this.mIsFullScreen = in.readInt() == 1;
        this.mPaintPixelSize = in.readFloat();
        this.mPaintUnitSize = in.readFloat();
        this.mMinScale = in.readFloat();
        this.mMaxScale = in.readFloat();
        this.mPaintColor = in.readInt();
        this.mSupportScaleItem = in.readInt() == 1;
        this.mOptimizeDrawing = in.readInt() == 1;

        return true;
    }


    public enum DialogType {
        SAVE, CLEAR_ALL, COLOR_PICKER;
    }

    public interface DialogInterceptor {
        /**
         * @param activity
         * @param doodle
         * @param dialogType 对话框类型
         * @return 返回true表示拦截
         */
        boolean onShow(Ability activity, IDoodle doodle, DialogType dialogType);
    }
}
