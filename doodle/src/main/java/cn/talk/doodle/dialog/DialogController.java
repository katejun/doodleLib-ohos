package cn.talk.doodle.dialog;

import cn.talk.doodle.ResourceTable;
import cn.talk.doodle.util.DrawUtil;
import cn.talk.doodle.util.StringUtils;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.*;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.service.WindowManager;


public class DialogController {
    public static CommonDialog showEnterCancelDialog(Ability slice, String title, String msg, final Component.ClickedListener enterClick, final Component.ClickedListener cancelClick) {
        return showMsgDialog(slice, title, msg, StringUtils.getString(slice, ResourceTable.String_doodle_cancel),
                StringUtils.getString(slice, ResourceTable.String_doodle_enter), enterClick, cancelClick);
    }

    public static CommonDialog showEnterDialog(Ability slice, String title, String msg, final Component.ClickedListener enterClick) {
        return showMsgDialog(slice, title, msg, null,
                StringUtils.getString(slice, ResourceTable.String_doodle_enter), enterClick, null);
    }

    public static CommonDialog showMsgDialog(Ability slice, String title, String msg, String btn01, String btn02, final Component.ClickedListener enterClick, final Component.ClickedListener cancelClick) {

        final CommonDialog dialog = getDialog(slice);
        dialog.getWindow().setLayoutInDisplaySideMode(WindowManager.LayoutConfig.INPUT_ADJUST_RESIZE);
//        StatusBarUtil.setStatusBarTranslucent(dialog.getWindow(), true, false);
        dialog.show();
        dialog.setAutoClosable(true);

        final CommonDialog finalDialog = dialog;

        Component view = LayoutScatter.getInstance(slice).parse(ResourceTable.Layout_doodle_dialog, null, false);
        dialog.setContentCustomComponent(view);

        if (StringUtils.isEmpty(title)) {
            view.findComponentById(ResourceTable.Id_dialog_title).setVisibility(Component.HIDE);
            view.findComponentById(ResourceTable.Id_dialog_list_title_divider).setVisibility(Component.HIDE);
        } else {
            Text titleView = (Text) view.findComponentById(ResourceTable.Id_dialog_title);
            titleView.setText(title);
        }

        if (StringUtils.isEmpty(msg)) {
            view.findComponentById(ResourceTable.Id_dialog_enter_msg).setVisibility(Component.HIDE);
        } else {
            Text titleView = (Text) view.findComponentById(ResourceTable.Id_dialog_enter_msg);
            titleView.setText(msg);
        }

        if (StringUtils.isEmpty(btn01)) {
            view.findComponentById(ResourceTable.Id_dialog_enter_btn_01).setVisibility(Component.HIDE);
        } else {
            Text textView = (Text) view.findComponentById(ResourceTable.Id_dialog_enter_btn_01);
            textView.setText(btn01);
        }

        if (StringUtils.isEmpty(btn02)) {
            view.findComponentById(ResourceTable.Id_dialog_enter_btn_02).setVisibility(Component.HIDE);
        } else {
            Text textView = (Text)view.findComponentById(ResourceTable.Id_dialog_enter_btn_02);
            textView.setText(btn02);
        }

        Component.ClickedListener onClickListener = new Component.ClickedListener() {
            public void onClick(Component v) {
                if (v.getId() == ResourceTable.Id_dialog_bg) {
                    finalDialog.hide();
                } else if (v.getId() == ResourceTable.Id_dialog_enter_btn_02) {
                    finalDialog.hide();
                    if (enterClick != null) {
                        enterClick.onClick(v);
                    }
                } else if (v.getId() ==ResourceTable.Id_dialog_enter_btn_01) {
                    finalDialog.hide();
                    if (cancelClick != null) {
                        cancelClick.onClick(v);
                    }
                }
            }
        };
        view.findComponentById(ResourceTable.Id_dialog_bg).setClickedListener(onClickListener);
        view.findComponentById(ResourceTable.Id_dialog_enter_btn_01).setClickedListener(onClickListener);
        view.findComponentById(ResourceTable.Id_dialog_enter_btn_02).setClickedListener(onClickListener);

        return dialog;
    }

    public static CommonDialog showInputTextDialog(Ability slice, final String text, final Component.ClickedListener enterClick, final Component.ClickedListener cancelClick) {

        boolean fullScreen = (slice.getWindow().getLayoutConfig().get().flags & WindowManager.LayoutConfig.MARK_FULL_SCREEN) != 0;
        final CommonDialog dialog = getDialog(slice);
        dialog.getWindow().setLayoutInDisplaySideMode(WindowManager.LayoutConfig.INPUT_ADJUST_RESIZE);
//        StatusBarUtil.setStatusBarTranslucent(dialog.getWindow(), true, false);
        dialog.show();

        Component container = LayoutScatter.getInstance(slice).parse(ResourceTable.Layout_doodle_create_text, null, false);

        container.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                dialog.hide();
            }
        });

        dialog.setContentCustomComponent(container);
        if (fullScreen) {
            DrawUtil.assistActivity(dialog.getWindow());
        }

        final TextField textView = (TextField) container.findComponentById(ResourceTable.Id_doodle_selectable_edit);
        final Component cancelBtn = container.findComponentById(ResourceTable.Id_doodle_text_cancel_btn);
        final Text enterBtn = (Text) container.findComponentById(ResourceTable.Id_doodle_text_enter_btn);

        textView.addTextObserver(new Text.TextObserver() {
            @Override
            public void onTextUpdated(String s, int i, int i1, int i2) {
                String text = (textView.getText() + "").trim();
                if (StringUtils.isEmpty(text)) {
                    enterBtn.setEnabled(false);
                    enterBtn.setTextColor(new Color(0xffb3b3b3));
                } else {
                    enterBtn.setEnabled(true);
                    enterBtn.setTextColor(new Color(0xff232323));
                }
            }
        });


        textView.setText(text == null ? "" : text);

        cancelBtn.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                dialog.hide();
                if (cancelClick != null) {
                    cancelClick.onClick(cancelBtn);
                }
            }
        });

        enterBtn.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                dialog.hide();
                if (enterClick != null) {
                    enterBtn.setTag((textView.getText() + "").trim());
                    enterClick.onClick(enterBtn);
                }
            }
        });

        return dialog;
    }

//    public static CommonDialog showSelectImageDialog(AbilitySlice slice, final ImageSelectorView.ImageSelectorListener listener) {
//
//        final CommonDialog dialog = getDialog(slice);
//        dialog.getWindow().setLayoutInDisplaySideMode(WindowManager.LayoutConfig.INPUT_ADJUST_RESIZE);
//        dialog.show();
//
//        Component container = LayoutScatter.getInstance(slice).parse(ResourceTable.Layout_doodle_create_bitmap, null, false);
//
//        container.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//            }
//        });
//        dialog.setContentView(container);
//
//        ViewGroup selectorContainer = (ViewGroup) dialog.findViewById(R.id.doodle_image_selector_container);
//        ImageSelectorView selectorView = new ImageSelectorView(activity, false, 1, null, new ImageSelectorView.ImageSelectorListener() {
//            @Override
//            public void onCancel() {
//                dialog.dismiss();
//                if (listener != null) {
//                    listener.onCancel();
//                }
//            }
//
//            @Override
//            public void onEnter(List<String> pathList) {
//                dialog.dismiss();
//                if (listener != null) {
//                    listener.onEnter(pathList);
//                }
//
//            }
//        });
//        selectorView.setColumnCount(4);
//        selectorContainer.addView(selectorView);
//        return dialog;
//    }

    private static CommonDialog getDialog(Ability slice) {
        boolean fullScreen = (slice.getWindow().getLayoutConfig().get().flags & WindowManager.LayoutConfig.MARK_FULL_SCREEN) != 0;
        CommonDialog dialog = null;
        if (fullScreen) {
            dialog = new CommonDialog(slice);
        } else {
            dialog = new CommonDialog(slice);
        }
        return dialog;
    }
}
