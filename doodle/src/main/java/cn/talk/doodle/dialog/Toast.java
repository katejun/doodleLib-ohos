package cn.talk.doodle.dialog;

import cn.talk.doodle.ResourceTable;
import ohos.agp.components.*;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.utils.TextTool;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;
import java.lang.ref.WeakReference;

/**
 * @author: mac
 * @date: 2020/12/29 11:14
 * @description: Toast工具类
 */
public final class Toast {


    private static int yOffset = 0;
    private static ToastDialog toast;

    private static final int LENGTH_SHORT = 0;
    private static final int LENGTH_LONG = 1500;
    private static WeakReference<Context> instance ;

    private Toast() {
    }

    public static void init(Context context) {
        if (instance == null || instance.get() == null) {
            instance = new WeakReference<>(context);
        }
    }

    public static void clear(){
        if (instance != null) {
            instance.clear();
        }
    }

    private static Context getInstance() {
        Context ct = instance.get();
        if (ct == null) {
            throw new RuntimeException("You should init before use. or pass context params in function");
        }
        return ct ;
    }

    /**
     * 显示土司
     *
     * @param msg 待显示的土司内容
     */
    public static void show(String msg) {
        show(msg, LENGTH_SHORT);
    }

    /**
     * 显示土司
     *
     * @param resId 待显示的土司资源id
     */
    public static void show(int resId) {
        show(resId, LENGTH_SHORT);
    }

    /**
     * 显示土司
     *
     * @param msg      待显示的土司内容
     * @param duration
     */
    public static void show(String msg, int duration) {
        show(getInstance(), msg, duration);
    }

    /**
     * 显示土司
     *
     * @param resId    待显示的土司资源id
     * @param duration
     */
    public static void show(int resId, int duration) {
        show(getInstance(), resId, duration);
    }

    /**
     * 显示土司
     *
     * @param msg 待显示的土司内容
     */
    public static void show(Context context, String msg) {
        show(context, msg, LENGTH_SHORT);
    }

    /**
     * 显示土司
     *
     * @param resId 待显示的土司资源id
     */
    public static void show(Context context, int resId) {
        show(context, resId, LENGTH_SHORT);
    }

    /**
     * 显示土司
     *
     * @param context
     * @param resId    待显示的土司资源id
     * @param duration
     */
    public static void show(Context context, int resId, int duration) {
        try {
            show(context, context.getResourceManager().getElement(resId).getString(), duration);
        } catch (IOException | NotExistException | WrongTypeException e) {
            e.printStackTrace();
        }
    }

    public static void show(Context context, int resId, Object... args) {
        try {
            show(context, String.format(context.getResourceManager().getElement(resId).getString(), args), LENGTH_SHORT);
        } catch (IOException | NotExistException | WrongTypeException e) {
            e.printStackTrace();
        }
    }

    public static void show(Context context, String format, Object... args) {
        show(context, String.format(format, args), LENGTH_SHORT);
    }

    public static void show(Context context, int resId, int duration, Object... args) {
        try {
            show(context, String.format(context.getResourceManager().getElement(resId).getString(), args), duration);
        } catch (IOException | NotExistException | WrongTypeException e) {
            e.printStackTrace();
        }
    }

    /**
     * 显示土司
     *
     * @param msg 待显示的土司内容
     */
    public static void showLong(String msg) {
        show(msg, LENGTH_LONG);
    }

    /**
     * 显示土司
     *
     * @param resId 待显示的土司资源id
     */
    public static void showLong(int resId) {
        show(resId, LENGTH_LONG);
    }

    public static void show(Context context, String format, int duration, Object... args) {
        show(context, String.format(format, args), duration);
    }

    public static void show(Context context, String msg, int duration) {
        if (TextTool.isNullOrEmpty(msg)) {
            return;
        }
        LayoutScatter inflate = LayoutScatter.getInstance(context);
        Component v = inflate.parse(ResourceTable.Layout_toast_layout, null, false);
        Text tvToast = (Text) v.findComponentById(ResourceTable.Id_tv_toast);
        tvToast.setText(msg);

        if (yOffset == 0) {
            yOffset = 100;
        }

        if (toast != null) {
            toast.cancel();
        }
        toast = new ToastDialog(context);
        toast.setAlignment(LayoutAlignment.HORIZONTAL_CENTER | LayoutAlignment.BOTTOM);
        toast.setOffset(0, yOffset);
        toast.setContentCustomComponent(v);
        toast.setDuration(duration);
        toast.show();
    }

    /**
     * 自定义布局（待完善）
     *
     * @param context
     * @return
     */
    private static Component createView(Context context) {
        DirectionalLayout root = new DirectionalLayout(context);
        DirectionalLayout.LayoutConfig rootConfig = new DirectionalLayout.LayoutConfig(DirectionalLayout.LayoutConfig.MATCH_CONTENT,
                DirectionalLayout.LayoutConfig.MATCH_CONTENT);
//        root.setBackground(Color.BLACK);
        root.setAlignment(LayoutAlignment.CENTER);
        root.setOrientation(Component.VERTICAL);
        root.setLayoutConfig(rootConfig);
        Text text = new Text(context);
        text.setTextSize(AttrHelper.fp2px(20f, context));
        text.setTextColor(Color.BLACK);
        root.addComponent(text);
        return root;
    }


    /**
     * 显示无网络时的提示
     */
    public static void showNoNetWorkToast() {
        show("检测到未连接网络");
    }

    /**
     * 显示默认网络出错的提示
     */
    public static void showDefaultFailToast() {
        show("网络请求出错,请稍后再试");
    }


    /**
     * 显示系统默认提示
     *
     * @param context
     * @param msg
     * @param duration
     */
    public static void showDefaultToast(Context context, String msg, int duration) {
        if (TextTool.isNullOrEmpty(msg)) {
            return;
        }
        if (duration > LENGTH_LONG) {
            duration = LENGTH_LONG;
        } else if (duration < LENGTH_SHORT) {
            duration = LENGTH_SHORT;
        }
        if (toast != null) {
            toast.cancel();
        }
        toast = new ToastDialog(context);
        toast.setDuration(duration);
        toast.setAlignment(LayoutAlignment.CENTER);
        toast.setText(msg);
        toast.show();

    }
}