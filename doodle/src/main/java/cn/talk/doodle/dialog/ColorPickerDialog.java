package cn.talk.doodle.dialog;

import cn.talk.doodle.ResourceTable;
import cn.talk.doodle.core.IDoodle;
import cn.talk.doodle.util.DensityUtils;
import cn.talk.doodle.util.DrawUtil;
import ohos.agp.components.*;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.CommonDialog;
import ohos.app.Context;

public class ColorPickerDialog extends CommonDialog {

    private final boolean debug = true;
    private final String TAG = "ColorPicker";

    Context context;
    private OnColorChangedListener mListener;

    /**
     * @param context
     * @param listener 回调
     */
    public ColorPickerDialog(Context context, OnColorChangedListener listener) {
        super(context);
        this.context = context;
        mListener = listener;
    }

    public void show(IDoodle iDoodle, Element drawable, int maxSize) {
        super.show();

        int height = DensityUtils.vp2px(context, 220);
        int width = DensityUtils.vp2px(context, 180);


        Component viewGroup = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_doodle_color_selector_dialog, null, false);

        final TextField sizeView = (TextField) viewGroup.findComponentById(ResourceTable.Id_doodle_txtview_size);
        final ProgressBar seekBar = (ProgressBar) viewGroup.findComponentById(ResourceTable.Id_doodle_seekbar_size);


        seekBar.setComponentStateChangedListener(new Component.ComponentStateChangedListener() {
            @Override
            public void onComponentStateChanged(Component component, int progress) {
                if (progress == 0) {
                    seekBar.setProgressValue(1);
                    return;
                }
                sizeView.setText("" + progress);
            }
        });

        seekBar.setMaxValue(maxSize);
        seekBar.setProgressValue((int) iDoodle.getSize());

        ComponentContainer container = (ComponentContainer) viewGroup.findComponentById(ResourceTable.Id_doodle_color_selector_container);
        ColorPickerView colorPickerView = new ColorPickerView(context, Color.BLACK, height, width, null);
        if (drawable instanceof PixelMapElement) {
            colorPickerView= new ColorPickerView(context, Color.BLACK, height, width, null);
            colorPickerView.setDrawable(colorPickerView.getDrawable());
        }
//        else if (drawable instanceof ) {
//            colorPickerView.setColor(((Element) drawable).getColor());
//        }
        container.addComponent(colorPickerView,0,new ComponentContainer.LayoutConfig(width,height));

        ColorPickerView finalColorPickerView = colorPickerView;
        Component.ClickedListener listener=new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Image imageView = (Image) component;
                finalColorPickerView.setDrawable((PixelMapElement) imageView.getImageElement());
            }
        };

        ComponentContainer shaderContainer = (ComponentContainer) viewGroup.findComponentById(ResourceTable.Id_doodle_shader_container);
        for (int i = 0; i < shaderContainer.getChildCount(); i++) {
            shaderContainer.getComponentAt(i).setClickedListener(listener);
        }

        sizeView.addTextObserver(new Text.TextObserver() {
            @Override
            public void onTextUpdated(String s, int i, int i1, int i2) {
                try {
                    int p = Integer.parseInt(s.toString());
                    if (p <= 0) {
                        p = 1;
                    }
                    if (p == seekBar.getProgress()) {
                        return;
                    }
                    seekBar.setProgressValue(p);
                    sizeView.setText("" + seekBar.getProgress());
//                    sizeView.setSelection(sizeView.getText().toString().length());
                } catch (Exception e) {
                }
            }
        });


        viewGroup.findComponentById(ResourceTable.Id_doodle_txtview_reduce).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                seekBar.setProgressValue(Math.max(1, seekBar.getProgress() - 1));
            }
        });
        viewGroup.findComponentById(ResourceTable.Id_doodle_txtview_add).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                seekBar.setProgressValue(Math.min(seekBar.getMax(), seekBar.getProgress() + 1));
            }
        });

        viewGroup.findComponentById(ResourceTable.Id_dialog_enter_btn_01).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                hide();
            }
        });
        ColorPickerView finalColorPickerView1 = colorPickerView;
        viewGroup.findComponentById(ResourceTable.Id_dialog_enter_btn_02).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                if (finalColorPickerView1.getDrawable() != null) {
                    mListener.colorChanged(finalColorPickerView1.getDrawable(), seekBar.getProgress());
                } else {
                    mListener.colorChanged(finalColorPickerView1.getColor(), seekBar.getProgress());
                }
                hide();
            }
        });

        setContentCustomComponent(viewGroup);
         setAutoClosable(false);

        DrawUtil.assistActivity(getWindow());

    }

    /**
     * 回调接口
     *
     * @author <a href="clarkamx@gmail.com">LynK</a>
     * <p/>
     * Create on 2012-1-6 上午8:21:05
     */
    public interface OnColorChangedListener {
        /**
         * 回调函数
         *
         * @param color 选中的颜色
         */
        void colorChanged(Color color, int size);

        void colorChanged(PixelMapElement color, int size);
    }

}
