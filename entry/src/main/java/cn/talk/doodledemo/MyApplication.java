package cn.talk.doodledemo;

import cn.talk.doodle.dialog.Toast;
import cn.talk.doodledemo.utils.log.LogUtils;
import ohos.aafwk.ability.AbilityPackage;

public class MyApplication extends AbilityPackage {
    @Override
    public void onInitialize() {
        super.onInitialize();

        LogUtils.init(this);
        Toast.init(this);
    }
}
