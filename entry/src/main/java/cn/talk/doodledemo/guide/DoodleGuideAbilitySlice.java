package cn.talk.doodledemo.guide;

import cn.talk.doodledemo.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;

/**
 * @author: SZS
 * @date: 2021-5-26
 * @describe:
 */
public class DoodleGuideAbilitySlice extends AbilitySlice {
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_guide);
    }
}
