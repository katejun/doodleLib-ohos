package cn.talk.doodledemo.guide;

import cn.talk.doodledemo.ResourceTable;
import cn.talk.doodledemo.slice.MainAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.ComponentContainer;

/**
 * @author: SZS
 * @date: 2021-5-26
 * @describe:
 */
public class DoodleGuideAbility extends Ability {
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(DoodleGuideAbilitySlice.class.getName());
    }

    @Override
    protected void onActive() {
        super.onActive();
        //初级涂鸦
        ComponentContainer simpleContainer =
                (ComponentContainer) findComponentById(ResourceTable.Id_container_simple_doodle);
        SimpleDoodleView simpleDoodleView = new SimpleDoodleView(this);
        simpleContainer.addComponent(simpleDoodleView,
                new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT,
                        ComponentContainer.LayoutConfig.MATCH_PARENT));

        //中级涂鸦
        ComponentContainer middleContainer =
                (ComponentContainer) findComponentById(ResourceTable.Id_container_middle_doodle);
        MiddleDoodleView middleDoodleView = new MiddleDoodleView(this);
        middleContainer.addComponent(middleDoodleView,
                new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT,
                        ComponentContainer.LayoutConfig.MATCH_PARENT));

        //高级涂鸦
        /*ComponentContainer advancedContainer =
                (ComponentContainer) findComponentById(ResourceTable.Id_container_advanced_doodle);
        SimpleDoodleView simpleDoodleView = new SimpleDoodleView(this);
        simpleContainer.addComponent(simpleDoodleView,
                new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT,
                        ComponentContainer.LayoutConfig.MATCH_PARENT));*/

    }
}
