package cn.talk.doodledemo.guide;

import cn.talk.doodle.ability.base.gesture.TouchGestureDetector;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: SZS
 * @date: 2021-5-25
 * @describe:
 */
public class SimpleDoodleView extends Component implements Component.DrawTask, Component.TouchEventListener ,
        MyTouchGestureListener.MyMoveGestureListener {
    private final static String TAG = "SimpleDoodleView";
    private Paint mPaint = new Paint();
    private List<Path> mPathList = new ArrayList<>(); // 保存涂鸦轨迹的集合
    private MyTouchGestureListener myTouchGestureListener;// 触摸手势监听
    private float mLastX, mLastY;
    private Path mCurrentPath; // 当前的涂鸦轨迹

    public SimpleDoodleView(Context context) {
        super(context);
        // 设置画笔
        initPaint();
        initListener();
    }
    private void initPaint(){
        mPaint.setColor(Color.RED);
        mPaint.setStyle(Paint.Style.STROKE_STYLE);
        mPaint.setStrokeWidth(20);
        mPaint.setAntiAlias(true);
        mPaint.setStrokeCap(Paint.StrokeCap.ROUND_CAP);
    }

    private void initListener(){
        setTouchEventListener(this);
        myTouchGestureListener = new MyTouchGestureListener(this);
        addDrawTask(this);
    }
    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        myTouchGestureListener.OnTouchEvent(touchEvent);
        return true;
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        for (Path path : mPathList) { // 绘制涂鸦轨迹
            canvas.drawPath(path, mPaint);
        }
    }

    @Override
    public boolean onScrollBegin(TouchEvent touchEvent) {
        MmiPoint pointerPosition = touchEvent.getPointerPosition(0);
        mCurrentPath = new Path(); // 新的涂鸦
        mPathList.add(mCurrentPath); // 添加的集合中
        mCurrentPath.moveTo(pointerPosition.getX(), pointerPosition.getY());
        mLastX = pointerPosition.getX();
        mLastY = pointerPosition.getY();
        invalidate(); // 刷新
        return false;
    }

    @Override
    public boolean onScroll(TouchEvent touchEvent) {
        MmiPoint pointerPosition = touchEvent.getPointerPosition(0);
        mCurrentPath.quadTo(
                mLastX,
                mLastY,
                (pointerPosition.getX() + mLastX) / 2,
                (pointerPosition.getY() + mLastY) / 2); // 使用贝塞尔曲线 让涂鸦轨迹更圆滑
        mLastX = pointerPosition.getX();
        mLastY = pointerPosition.getY();
        invalidate(); // 刷新
        return false;
    }

    @Override
    public boolean onScrollEnd(TouchEvent touchEvent) {
        MmiPoint pointerPosition = touchEvent.getPointerPosition(0);
        mCurrentPath.quadTo(
                mLastX,
                mLastY,
                (pointerPosition.getX() + mLastX) / 2,
                (pointerPosition.getY() + mLastY) / 2); // 使用贝塞尔曲线 让涂鸦轨迹更圆滑
        mCurrentPath = null; // 轨迹结束
        invalidate(); // 刷新
        return false;
    }
}
