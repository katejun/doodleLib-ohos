package cn.talk.doodledemo.guide;

import ohos.agp.render.Path;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: SZS
 * @date: 2021-5-28
 * @describe:
 */
public class MyTouchGestureListener {
    private float mLastX, mLastY;
    private Path mCurrentPath; // 当前的涂鸦轨迹
    private List<Path> mPathList = new ArrayList<>(); // 保存涂鸦轨迹的集合
    private MyMoveGestureListener myMoveGestureListener;

    public  MyTouchGestureListener(MyMoveGestureListener myMoveGestureListener) {
        this.myMoveGestureListener = myMoveGestureListener;
    }

    public boolean OnTouchEvent(TouchEvent touchEvent) {
        MmiPoint pointerPosition = touchEvent.getPointerPosition(0);
        switch (touchEvent.getAction()) {
            case TouchEvent.PRIMARY_POINT_DOWN:
                /*mCurrentPath = new Path(); // 新的涂鸦
                mPathList.add(mCurrentPath); // 添加的集合中
                mCurrentPath.moveTo(pointerPosition.getX(), pointerPosition.getY());
                mLastX = pointerPosition.getX();
                mLastY = pointerPosition.getY();*/
                if (myMoveGestureListener != null) {
                    myMoveGestureListener.onScrollBegin(touchEvent);
                }
                break;
            case TouchEvent.POINT_MOVE:
                /*mCurrentPath.quadTo(
                        mLastX,
                        mLastY,
                        (pointerPosition.getX() + mLastX) / 2,
                        (pointerPosition.getY() + mLastY) / 2); // 使用贝塞尔曲线 让涂鸦轨迹更圆滑
                mLastX = pointerPosition.getX();
                mLastY = pointerPosition.getY();*/
                if (myMoveGestureListener != null) {
                    myMoveGestureListener.onScroll(touchEvent);
                }
                break;
            case TouchEvent.PRIMARY_POINT_UP:
                /*mCurrentPath.quadTo(
                        mLastX,
                        mLastY,
                        (pointerPosition.getX() + mLastX) / 2,
                        (pointerPosition.getY() + mLastY) / 2); // 使用贝塞尔曲线 让涂鸦轨迹更圆滑
                mCurrentPath = null; // 轨迹结束*/
                if (myMoveGestureListener != null) {
                    myMoveGestureListener.onScrollEnd(touchEvent);
                }
                break;
        }
        return true;
    }

    public interface MyMoveGestureListener {

        boolean onScrollBegin(TouchEvent touchEvent);

        boolean onScroll(TouchEvent e);

        boolean onScrollEnd(TouchEvent touchEvent);
    }
}
