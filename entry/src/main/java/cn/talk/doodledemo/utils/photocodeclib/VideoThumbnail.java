package cn.talk.doodledemo.utils.photocodeclib;

import ohos.media.image.PixelMap;
import ohos.media.image.common.Size;

import java.io.File;

/**
 * @version 1.0.1
 * @description: 视频生成缩略图
 * @program: Gallery
 * @Author xiaozhijun
 * @Date 2020/12/22 14:43
 */
public class VideoThumbnail extends IThumbnail {


    @Override
    public PixelMap getThumbPixMap(File videoFile, Size size) {
        return createVideoThumbnail(videoFile, size);
    }

    /**
     * 获取视频第一帧图像创建缩略图
     *
     * @param file
     * @param size
     * @return
     */
    private PixelMap createVideoThumbnail(File file, Size size) {
        VideoExifInfoRequestUtils utils = new VideoExifInfoRequestUtils(file.getAbsolutePath());
        PixelMap pixelMap = utils.fetchVideoPixelMapByIndex(1);
        Size autoSize = size;
        Size sourceSize = pixelMap.getImageInfo().size;
        if (sourceSize != null) {
            autoSize = ThumbPhotoSizeUtils.computeAutoSize(sourceSize, size);
            autoSize = autoSize != null ? autoSize : size;
        }
        PixelMap.InitializationOptions in = new PixelMap.InitializationOptions();
        in.size = autoSize;
        PixelMap result = null;
        if (pixelMap != null) {
            result = PixelMap.create(pixelMap, in);
            pixelMap.release();
        }
        return result;
    }
}
