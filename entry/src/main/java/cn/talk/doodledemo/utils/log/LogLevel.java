package cn.talk.doodledemo.utils.log;

import cn.talk.doodledemo.annotation.IntDef;
import ohos.hiviewdfx.HiLog;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class LogLevel {
    public static final int TYPE_VERBOSE = HiLog.LOG_APP;
    public static final int TYPE_DEBUG = HiLog.DEBUG;
    public static final int TYPE_INFO = HiLog.INFO;
    public static final int TYPE_WARM = HiLog.WARN;
    public static final int TYPE_ERROR = HiLog.ERROR;
    public static final int TYPE_WTF = HiLog.FATAL;

    @IntDef({TYPE_VERBOSE, TYPE_DEBUG, TYPE_INFO, TYPE_WARM, TYPE_ERROR})
    @Retention(RetentionPolicy.SOURCE)
    public @interface LogLevelType {
    }
}
