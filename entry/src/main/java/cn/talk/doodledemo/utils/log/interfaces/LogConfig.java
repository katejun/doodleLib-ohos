package cn.talk.doodledemo.utils.log.interfaces;


import cn.talk.doodledemo.utils.log.LogLevel;
import cn.talk.doodledemo.utils.log.LogUtils;

public interface LogConfig {

    /**
     * 日志开关
     * @param allowLog true：开启 false：关闭
     * @return
     */
    LogConfig configAllowLog(boolean allowLog);

    /**
     * 日志前缀 方便跟踪筛选日志信息
     * @param prefix
     * @return
     */
    LogConfig configTagPrefix(String prefix);

    /**
     * 日志打印边框显示
     * @param showBorder  true：显示日志边框  false:不显示边框
     * @return
     */
    LogConfig configShowBorders(boolean showBorder);

    /**
     * 日志打印等级限定
     * @param logLevel  低于该等级不打印
     * @return
     */
    LogConfig configLevel(@LogLevel.LogLevelType int logLevel);

    /**
     * 配置日志内核  目前有：HiLog和Logger两种
     * @param coreType  默认为HiLog
     * @return
     */
    LogConfig configLogCore(@LogUtils.CoreType.Type int coreType);

    /**
     * 配置日志文件的目录路径，注意：是目录路径而非文件路径
     * @param path  目录路径
     * @return
     */
    LogConfig configLogDirPath(String path);

    /**
     * 数据库日志存储等级限定
     * @param logLevel  低于该等级不存储
     * @return
     */
    LogConfig configDbLevel(@LogLevel.LogLevelType int logLevel);
}
