package cn.talk.doodledemo.utils.log;

import java.util.logging.Level;
import java.util.logging.Logger;

class Logcat extends AbstractPrinter {

    private static Logcat instance;

    public static Logcat getInstance() {
        if (instance == null) {
            synchronized (Logcat.class) {
                if (instance == null) {
                    instance = new Logcat();
                }
            }
        }

        return instance;
    }
    private final LogConfigImpl mLogConfig;

    private Logcat() {
        mLogConfig = LogConfigImpl.getInstance();
    }

    @Override
    public void printLog(int type, String tag, String msg) {
        if (!mLogConfig.isShowBorder()) {
            msg = getTopStackInfo() + ": " + msg;
        }
        switch (type) {
            case LogLevel.TYPE_VERBOSE:
                Logger.getLogger(tag).log(Level.ALL, msg);
                break;
            case LogLevel.TYPE_DEBUG:
                Logger.getLogger(tag).log(Level.INFO, msg);
                break;
            case LogLevel.TYPE_INFO:
                Logger.getLogger(tag).log(Level.INFO, msg);
                break;
            case LogLevel.TYPE_WARM:
                Logger.getLogger(tag).log(Level.WARNING, msg);
                break;
            case LogLevel.TYPE_ERROR:
                Logger.getLogger(tag).log(Level.SEVERE, msg);
                break;
            case LogLevel.TYPE_WTF:
                Logger.getLogger(tag).log(Level.SEVERE, msg);
                break;
            default:
                break;
        }
    }
}
