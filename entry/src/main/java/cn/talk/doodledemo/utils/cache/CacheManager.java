package cn.talk.doodledemo.utils.cache;

import cn.talk.doodledemo.utils.FileUtils;
import cn.talk.doodledemo.utils.cache.disk.DiskCacheUtils;
import cn.talk.doodledemo.utils.cache.disk.DiskLruCache;
import cn.talk.doodledemo.utils.cache.memory.LruCache;
import cn.talk.doodledemo.utils.cache.memory.MemoryCacheUtils;
import cn.talk.doodledemo.utils.photocodeclib.ThumbnailFactory;
import ohos.app.Context;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.Size;

import java.io.File;
import java.io.IOException;

/**
 * @author 刘建久
 * @description 缓存操作管理类
 * @date 2020/12/24
 */
public class CacheManager {

    //默认硬盘存储大小
    public static final int DE_DISK_SIZE = 100*1024*1024;
    //默认内存大小
    public static final int DE_ME_SIZE = 50*1024*1024;
    private static CacheManager instance;
    private Context context;
    private CacheConfig cacheConfig;
    private LruCache<String, PixelMap> lruCache;
    private DiskLruCache diskLruCache;

    private CacheManager() {
    }

    public void init(Context mContext, CacheConfig cacheConfig) throws IOException {
        this.context = mContext;
        if (cacheConfig != null){
            this.cacheConfig = cacheConfig;
        } else {
            CacheConfig.Builder builder = new CacheConfig.Builder();
            builder.diskCacheSize(DE_DISK_SIZE)
                    .memoryCacheSize(DE_ME_SIZE)
                    .useDiskCache(true)
                    .useMemoryCache(true)
                    .diskFilePath(FileUtils.getAppCacheDir(context));
            this.cacheConfig = builder.build();
            lruCache = new LruCache<String, PixelMap>(this.cacheConfig.getMemoryCacheSize()) {
                @Override
                protected int sizeOf(String key, PixelMap value) {
                    //在每次存入缓存的时候调用
                    return (int) value.getPixelBytesNumber();
                }
            };
            diskLruCache = DiskLruCache.open(new File(this.cacheConfig.getDiskFilePath()), 1, 1, this.cacheConfig.getDiskCacheSize());
        }
    }


    public static CacheManager getInstance() {
        if (instance == null) {
            synchronized (CacheManager.class) {
                if (instance == null) {
                    instance = new CacheManager();
                }
            }
        }
        return instance;
    }


    /**
     * 存入位图照片
     * @param path 路径
     * @param pixelMap 位图
     * @return
     */
    public void put(String path,PixelMap pixelMap) {
        if (cacheConfig.isUseMemoryCache()) {
            MemoryCacheUtils.put(path,pixelMap,lruCache);
        }
        if (cacheConfig.isUseDiskCache()) {
            DiskCacheUtils.putDataToDiskCache(path,pixelMap,diskLruCache);
        }
    }

    /**
     * 获取图片原图，或者图片缩略图，或者视频封面
     * @param path 文件绝对路径
     * @param width 缩略图宽度，<=0时返回原图。
     * @param height 缩略图高度，<=0时返回原图。
     * @param isVideo 是否是视频。
     * @return 图片原图，或者图片缩略图，或者视频封面
     */
    public PixelMap get(String path,int width,int height,boolean isVideo) {
        PixelMap pixelMap;
        String cachePath = path+"w_"+width+"h_"+height;
        if (cacheConfig.isUseMemoryCache()) {
            pixelMap = MemoryCacheUtils.get(cachePath,lruCache);;
            if(pixelMap!=null){//内存中取到
                return pixelMap;
            }
        }
        if (cacheConfig.isUseDiskCache()) {
            pixelMap = DiskCacheUtils.getDataFromDiskCache(cachePath,diskLruCache);
            if(pixelMap!=null){//外存中取到
                if (cacheConfig.isUseMemoryCache()) {
                    //存入内存
                    MemoryCacheUtils.put(cachePath,pixelMap,lruCache);
                }
                return pixelMap;
            }
        }

        //内存缓存和外存缓存中都未找到，检索磁盘
        if(isVideo){//视频封面
            Size size = new Size(width,height);
            File file = new File(path);
            pixelMap = ThumbnailFactory.getInstance().createVideoThumbMap(file,size);
        }else if(width>0 && height>0){//图片缩略图
            Size size = new Size(width,height);
            File file = new File(path);
            pixelMap = ThumbnailFactory.getInstance().createPhotoThumbMap(file,size);
        }else{//原图
            ImageSource imageSourceNoOptions = ImageSource.create(path, null);
            // 普通解码
            pixelMap = imageSourceNoOptions.createPixelmap(null);
        }

        //存入缓存
        put(cachePath,pixelMap);
        return pixelMap;
    }

    /**
     * 清空缓存
     */
    public void clear(){
        if(lruCache!=null&& cacheConfig!=null && cacheConfig.isUseMemoryCache()){
            lruCache.evictAll();
        }
        if(diskLruCache!=null&& cacheConfig!=null && cacheConfig.isUseDiskCache()){
            try{
                diskLruCache.delete();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}
