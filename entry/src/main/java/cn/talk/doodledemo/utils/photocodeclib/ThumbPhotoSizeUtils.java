package cn.talk.doodledemo.utils.photocodeclib;

import cn.talk.doodledemo.utils.log.LogUtils;
import ohos.media.image.ImageSource;
import ohos.media.image.common.Size;

import java.io.File;

/**
 * @version 1.0.1
 * @description:图像压缩尺寸获取
 * @program: Gallery
 * @Author xiaozhijun
 * @Date 2021/1/14 17:14
 */
public class ThumbPhotoSizeUtils {

    /**
     * 自动等比压缩 Size
     *
     * @param imageSource 源图像 imageSource
     * @param size        期待输出尺寸
     * @return 最终输出的等比压缩尺寸
     */
    protected static Size autoCompressSize(ImageSource imageSource, Size size) {
        Size result = null;
        if (imageSource != null) {
            result = new Size(200, 200);
            if (imageSource.getImageInfo() != null && imageSource.getImageInfo().size != null) {
                Size sourceSize = imageSource.getImageInfo().size;
                result = computeAutoSize(sourceSize, size);
            } else {
                LogUtils.i("autoCompressSize get Source Size is Fail");
            }
        } else {
            LogUtils.i("autoCompressSize get imageSource is Null");
        }
        return result;
    }

    /**
     * 根据图像原尺寸和输出尺寸，基于输出最小长度作等比压缩输出尺寸
     *
     * @param sourceSize 源图像尺寸
     * @param size       期待输出尺寸
     * @return 最终输出的等比压缩尺寸
     */
    static Size computeAutoSize(Size sourceSize, Size size) {
        Size result = null;
        if (sourceSize != null) {
            result = new Size();
            if (sourceSize.width < size.width) {
                size.width = sourceSize.width;
            }
            if (sourceSize.height < size.height) {
                size.height = sourceSize.height;
            }
            LogUtils.i("autoCompressSize sourceSize is " + sourceSize);
            if (sourceSize.width > sourceSize.height) {
                if (size != null) {
                    result.height = size.height;
                    result.width = result.height * sourceSize.width / sourceSize.height;
                } else {
                    result.height = IThumbnail.THUMB_DEFAULT_SIZE;
                    result.width = result.height * sourceSize.width / sourceSize.height;
                }
            } else {
                if (size != null) {
                    result.width = size.width;
                    result.height = result.width * sourceSize.height / sourceSize.width;
                } else {
                    result.width = IThumbnail.THUMB_DEFAULT_SIZE;
                    result.height = size.width * sourceSize.height / sourceSize.width;
                }
            }
            LogUtils.i("autoCompressSize result is " + result);
        }
        return result;
    }

    /**
     * 根据传输参数 修改压缩比尺寸
     *
     * @param f          文件
     * @param size       期待输出尺寸
     * @param isTooLarge 是否是超分辨率图像
     * @return 最终输出的等比压缩尺寸
     */
    protected static Size autoCompressSize(File f, Size size, boolean isTooLarge) {
        Size autoSize = null;
        if (isTooLarge) {
            Size decodeSize = PhotoDecode.decodePhotoInJustBound(f);
            autoSize = computeAutoSize(decodeSize, size);
        } else {
            ImageSource imageSource = ImageSource.create(f, null);
            autoSize = autoCompressSize(imageSource, size);
        }
        return autoSize;
    }

}
