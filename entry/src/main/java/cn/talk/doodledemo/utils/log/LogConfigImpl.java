package cn.talk.doodledemo.utils.log;

import cn.talk.doodledemo.utils.log.interfaces.LogConfig;

/**
 * Log config
 */
class LogConfigImpl implements LogConfig {

    private boolean enable = true;
    private String tagPrefix;
    private boolean showBorder = true;
    private int logLevel = LogLevel.TYPE_VERBOSE;
    /**
     * 数据库存储等级
     * 默认低于TYPE_WARM不存储
     */
    private int logDbLevel = LogLevel.TYPE_WARM;

    private int logCoreType;
    /**
     * 日志留存时间
     * 单位：天
     */
    private int saveDay = 30;

    public int getSaveDay() {
        return saveDay;
    }

    public void setSaveDay(int saveDay) {
        this.saveDay = saveDay;
    }

    /**
     * specify file dir
     */
    private String logDir;
    /**
     * default file dir
     */
    private String defaultLogDir;

    private static LogConfigImpl singleton;

    private LogConfigImpl() {
    }

    void setDefaultLogDir(String dir) {
        defaultLogDir = dir;
    }

    static LogConfigImpl getInstance() {
        if (singleton == null) {
            synchronized (LogConfigImpl.class) {
                if (singleton == null) {
                    singleton = new LogConfigImpl();
                }
            }
        }
        return singleton;
    }

    @Override
    public LogConfig configAllowLog(boolean allowLog) {
        this.enable = allowLog;
        return this;
    }

    @Override
    public LogConfig configTagPrefix(String prefix) {
        this.tagPrefix = prefix;
        return this;
    }

    @Override
    public LogConfig configShowBorders(boolean showBorder) {
        this.showBorder = showBorder;
        return this;
    }

    @Override
    public LogConfig configLogCore(int coreType) {
        logCoreType = coreType;
        return this;
    }

    @Override
    public LogConfig configLogDirPath(String path) {
        if (!StringUtils.isEmpty(path)) {
            logDir = path;
        }

        return this;
    }

    @Override
    public LogConfig configDbLevel(int logLevel) {
        logDbLevel = logLevel;
        return this;
    }

    public int getLogDbLevel() {
        return logDbLevel;
    }

    public String getLogDir() {
        if (!StringUtils.isEmpty(logDir)) {
            return logDir;
        }

        return defaultLogDir;
    }

    @Override
    public LogConfig configLevel(@LogLevel.LogLevelType int logLevel) {
        this.logLevel = logLevel;
        return this;
    }

    boolean isEnable() {
        return enable;
    }

    String getTagPrefix() {
        if (StringUtils.isEmpty(tagPrefix)) {
            return "LogUtils";
        }

        return tagPrefix;
    }

    boolean isShowBorder() {
        return showBorder;
    }

    int getLogCoreType() {
        return logCoreType;
    }

    int getLogLevel() {
        return logLevel;
    }
}
