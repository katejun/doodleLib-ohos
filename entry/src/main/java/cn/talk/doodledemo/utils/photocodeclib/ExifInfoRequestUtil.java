package cn.talk.doodledemo.utils.photocodeclib;

import ohos.media.image.ImageSource;

/**
 * @author SUQI
 * @date 2020/12/30
 * @description 图片exif信息获取
 **/
public class ExifInfoRequestUtil {

    /**
     * exif信息获取
     * @param imageSource
     * PropertyKey.Exif.ISO_SPEED_RATINGS iso感光值
     * PropertyKey.Exif.FOCAL_LENGTH    焦距
     * PropertyKey.Exif.EXPOSURE_TIME   曝光时间
     * PropertyKey.Exif.WHITE_BALANCE   白平衡：0-自动
     * PropertyKey.Exif.FLASH           闪光灯：0-关闭
     * PropertyKey.Exif.APERTURE_VALUE  光圈
     * @param exifKey
     * @return
     */
    public static String getExifValue(ImageSource imageSource, String exifKey) {
        return imageSource.getImagePropertyString(exifKey);
    }
}
