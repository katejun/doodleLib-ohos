package cn.talk.doodledemo.utils.photocodeclib;

import ohos.media.image.PixelMap;
import ohos.media.image.common.Size;

import java.io.File;

/**
 * @version 1.0.1
 * @description: 媒体数据生成缩略图
 * @program: Gallery
 * @Author xiaozhijun
 * @Date 2020/12/22 14:43
 */
public class ThumbnailFactory {

    private static class ThumbnailManagerHolder {
        public static ThumbnailFactory manager = new ThumbnailFactory();
    }


    private ThumbnailFactory() {
        photoThumbnail = new PhotoThumbnail();
        videoThumbnail = new VideoThumbnail();
    }

    public static ThumbnailFactory getInstance() {
        return ThumbnailManagerHolder.manager;
    }


    private PhotoThumbnail photoThumbnail;

    private VideoThumbnail videoThumbnail;

    /**
     * 创建 视频缩略图
     *
     * @param videofile 视频文件
     * @param cacheDir  缩略图缓存目录
     * @param name      缩略图缓存文件名
     * @param size      保存的错略图文件代销
     * @return 缩略图路径 为NUll/为空时生成失败
     */
    public String createVideoThumbnail(File videofile, String cacheDir, String name, Size size) {
        return videoThumbnail.createThumbnail(videofile, cacheDir, name, size);
    }

    /**
     * 创建 视频缩略图
     *
     * @param videofile 视频文件
     * @param size      保存的错略图文件代销
     * @return 缩略图路径 为NUll/为空时生成失败
     */
    public PixelMap createVideoThumbMap(File videofile,Size size) {
        return videoThumbnail.getThumbPixMap(videofile, size);
    }

    /**
     * 创建 图片缩略图
     *
     * @param imageFile 图片文件
     * @param cacheDir  缩略图缓存目录
     * @param name      缩略图缓存文件名
     * @param size      保存的错略图文件代销
     * @return 缩略图路径 为NUll/为空时生成失败
     */
    public String createPhotoThumbnail(File imageFile, String cacheDir, String name, Size size) {
        return photoThumbnail.createThumbnail(imageFile, cacheDir,name, size);
    }

    /**
     * 创建 图片缩略图
     *
     * @param imageFile 图片文件
     * @param size      保存的错略图文件代销
     * @return 缩略图 为NUll时生成失败
     */
    public PixelMap createPhotoThumbMap(File imageFile, Size size) {
        return photoThumbnail.getThumbPixMap(imageFile, size);
    }

}
