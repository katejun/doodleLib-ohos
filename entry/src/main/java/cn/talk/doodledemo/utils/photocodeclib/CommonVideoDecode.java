package cn.talk.doodledemo.utils.photocodeclib;

import ohos.media.codec.Codec;
import ohos.media.codec.TrackInfo;
import ohos.media.common.Format;
import ohos.media.common.Source;

import java.io.FileDescriptor;

/**
 * @version 1.0.1
 * @description:视频管道模式解码
 * @program: Gallery
 * @Author xiaozhijun
 * @Date 2020/12/24 11:07
 */
public class CommonVideoDecode implements IVideoDecode<FileDescriptor> {

    //视频格式
    private Format videoFormat;

    //解码监听
    private Codec.ICodecListener codecListener;

    //标识解码
    private boolean isCodecing = false;

    //解码类
    private Codec codec;

    private CommonVideoDecode(Format fmt) {
        this.videoFormat = fmt;
    }

    private CommonVideoDecode(Format fmt, Codec.ICodecListener listener) {
        this.videoFormat = fmt;
        this.codecListener = listener;
    }

    @Override
    public void startVideDecode(FileDescriptor f) {
        try {
            codec = Codec.createDecoder();
            Source source = new Source(f);
            TrackInfo trackInfo = new TrackInfo();
            codec.setSource(source, trackInfo);
            if (codecListener != null) {
                codec.registerCodecListener(codecListener);
            }
            codec.setCodecFormat(videoFormat);
            isCodecing = true;
            codec.start();
        } catch (Exception e) {
            e.printStackTrace();
            isCodecing = false;
        }

    }

    @Override
    public void stopVideDecode() {
        try {
            if (isCodecing) {
                codec.stop();
                codec.release();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        isCodecing = false;
    }


    public static class Builder {

        private Format fmt;

        Codec.ICodecListener listener;

        public Builder() {
            fmt = new Format();
            setFrameInterval(-1);
        }

        /**
         * 设置解码格式
         *
         * @param mime 格式
         */
        public Builder setMime(String mime) {
            fmt.putStringValue(Format.MIME, mime);
            return this;
        }

        /**
         * 设置解码视频尺寸
         *
         * @param width  宽
         * @param height 高
         */
        public Builder setSize(int width, int height) {
            fmt.putIntValue(Format.WIDTH, width);
            fmt.putIntValue(Format.HEIGHT, height);
            return this;
        }

        /**
         * 设置比特率
         *
         * @param bit 比特率
         */
        public Builder setBitRate(int bit) {
            fmt.putIntValue(Format.BIT_RATE, bit);
            return this;
        }

        /**
         * 设置帧速率
         *
         * @param frame 帧速率
         */
        public Builder setFrameRate(int frame) {
            fmt.putIntValue(Format.FRAME_RATE, frame);
            return this;
        }


        /**
         * 设置帧间隔（默认设置为-1）
         *
         * @param frameInterval 帧间隔
         */
        public Builder setFrameInterval(int frameInterval) {
            fmt.putIntValue(Format.FRAME_INTERVAL, frameInterval);
            return this;
        }

        /**
         * 设置解码监听
         *
         * @param listen 监听器
         */
        public Builder setCodecListener(Codec.ICodecListener listen) {
            this.listener = listen;
            return this;
        }


        public CommonVideoDecode build() {
            return new CommonVideoDecode(fmt, listener);
        }
    }


}
