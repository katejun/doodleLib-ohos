package cn.talk.doodledemo.utils.photocodeclib;

import cn.talk.doodledemo.utils.log.LogUtils;
import ohos.agp.utils.TextTool;
import ohos.media.image.PixelMap;
import ohos.media.image.common.Size;

import java.io.File;

/**
 * @version 1.0.1
 * @description: 生成缩略图的抽象接口
 * @program: Gallery
 * @Author xiaozhijun
 * @Date 2020/12/22 14:43
 */
public abstract class IThumbnail {

    private static final String TAG = "IThumbnail";

    protected static final int THUMB_DEFAULT_SIZE = 200;

    /***
     * 创建缩略图
     * @param f 需要生成缩略图的文件目标
     * @param cacheDir 缓存缩略图的目录
     * @param name 缓存的缩略图名字
     * @param size 生成缩略图的尺寸
     * @return 生成的缓存缩略图路径，为空则表示生成失败
     */
    public String createThumbnail(File f, String cacheDir, String name, Size size) {
        String fileDir = cacheDir;
        if (TextTool.isNullOrEmpty(fileDir)) {
            return fileDir;
        }
        File file = new File(fileDir);
        if (!file.exists()) {
            boolean isMkdirs = file.mkdirs();
            LogUtils.i("create cacheDir result is " + isMkdirs);
        }
        if (fileDir.endsWith("/")) {
            fileDir = fileDir + name;
        } else {
            fileDir = fileDir + "/" + name;
        }
        PixelMap thumbnail = getThumbPixMap(f, size);
        boolean isDecodeSuccess = new PhotoEncode.Bulider().build().encodePhoto(fileDir, thumbnail);
        if (!isDecodeSuccess) {
            fileDir = "";
        }
        return fileDir;
    }

    /**
     * 获取缩略图 图像
     * @param f    图像文件
     * @param size 要生成的图像大小
     * @return PixelMap 位图对象
     */
    protected abstract PixelMap getThumbPixMap(File f, Size size);


}
