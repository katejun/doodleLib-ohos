package cn.talk.doodledemo.utils.photocodeclib;

/**
 * @description:视频解码接口
 * @program: Gallery
 * @Author xiaozhijun
 * @Date 2020/12/24 11:13
 * @version 1.0.1
 */
public interface IVideoDecode<T> {

    /**
     * 开始视频解码
     *
     * @param t 视频源(可以是文件描述符、uri中的任意一种)
     */
    public void startVideDecode(T t);

    /**
     * 停止解码
     */
    public void stopVideDecode();
}
