package cn.talk.doodledemo.utils.photocodeclib;

import ohos.media.image.PixelMap;

/**
 * @version 1.0.1
 * @description: 图片解码接口
 * @program: Gallery
 * @Author xiaozhijun
 * @Date 2021/1/7 17:05
 */
public interface IPhotoDecode {

    public PixelMap commonDecodePhoto(String filepath);
}
