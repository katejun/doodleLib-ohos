package cn.talk.doodledemo.utils.photocodeclib;

import cn.talk.doodledemo.utils.log.LogUtils;
import ohos.agp.utils.TextTool;
import ohos.media.image.PixelMap;
import ohos.media.photokit.metadata.AVMetadataHelper;

/**
 * @version 1.0.1
 * @description: 获取视频信息
 * @program: Gallery
 * @Author xiaozhijun
 * @Date 2021/1/8 9:59
 */
public class VideoExifInfoRequestUtils {

    private AVMetadataHelper avMetadataHelper;

    String videoPath;

    public VideoExifInfoRequestUtils(String videoPath) {
        avMetadataHelper = new AVMetadataHelper();
        setVideoPath(videoPath);
    }

    public void setVideoPath(String videoPath) {
        avMetadataHelper.setSource(videoPath);
        this.videoPath = videoPath;
    }

    /**
     * 根据key获取对应的属性
     *
     * @param key AVMetadataHelper.AV_KEY_DURATION 时常
     *            AVMetadataHelper.AV_KEY_VIDEO_WIDTH 视频宽
     *            AVMetadataHelper.AV_KEY_VIDEO_HEIGHT 视频高
     *            ....
     * @return
     */
    public String getVideoExifInfo(int key) {
        if (TextTool.isNullOrEmpty(videoPath)) {
            LogUtils.e("It can be used normally only when the videopath is not empty");
            return "";
        }
        return avMetadataHelper.resolveMetadata(key);
    }

    /**
     * 获取视频源中指定一帧的数据
     * @param frameIndex
     * @return
     */
    public PixelMap fetchVideoPixelMapByIndex(int frameIndex) {
        if (TextTool.isNullOrEmpty(videoPath)) {
            LogUtils.e("It can be used normally only when the videopath is not empty");
            return null;
        }
        return avMetadataHelper.fetchVideoPixelMapByIndex(frameIndex);
    }
    public void release() {
        if (avMetadataHelper != null) {
            avMetadataHelper.release();
        }
    }

}
