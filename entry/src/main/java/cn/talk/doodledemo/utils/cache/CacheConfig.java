package cn.talk.doodledemo.utils.cache;

/**
 * @author 刘建久
 * @description 缓存信息配置类
 * @date 2020/12/24
 */
public class CacheConfig {
    //内存空间大小
    private int memoryCacheSize;
    //磁盘空间大小
    private int diskCacheSize;
    //是否使用内存缓存
    private boolean useMemoryCache;
    //是否使用磁盘缓存
    private boolean useDiskCache;
    //磁盘缓存路径
    private String diskFilePath;

    private CacheConfig(int memoryCacheSize, int diskCacheSize, boolean useMemoryCache, boolean useDiskCache, String diskFilePath) {
        this.memoryCacheSize = memoryCacheSize;
        this.diskCacheSize = diskCacheSize;
        this.useMemoryCache = useMemoryCache;
        this.useDiskCache = useDiskCache;
        this.diskFilePath = diskFilePath;
    }

    public static class Builder {
        private int memoryCacheSize;
        private int diskCacheSize;
        private boolean useMemoryCache;
        private boolean useDiskCache;
        private String diskFilePath;

        Builder memoryCacheSize(int memoryCacheSize) {
            this.memoryCacheSize = memoryCacheSize;
            return this;
        }

        Builder diskCacheSize(int diskCacheSize) {
            this.diskCacheSize = diskCacheSize;
            return this;
        }

        Builder useMemoryCache(boolean useMemoryCache) {
            this.useMemoryCache = useMemoryCache;
            return this;
        }

        Builder useDiskCache(boolean useDiskCache) {
            this.useDiskCache = useDiskCache;
            return this;
        }

        Builder diskFilePath(String diskFilePath) {
            this.diskFilePath = diskFilePath;
            return this;
        }

        public CacheConfig build() {
            return new CacheConfig(memoryCacheSize,diskCacheSize,useMemoryCache,useDiskCache,diskFilePath);
        }
    }


    public int getMemoryCacheSize() {
        return memoryCacheSize;
    }

    public int getDiskCacheSize() {
        return diskCacheSize;
    }

    public boolean isUseMemoryCache() {
        return useMemoryCache;
    }

    public boolean isUseDiskCache() {
        return useDiskCache;
    }

    public String getDiskFilePath() {
        return diskFilePath;
    }
}

