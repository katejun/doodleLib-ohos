package cn.talk.doodledemo.utils.log;

import cn.talk.doodledemo.utils.cache.memory.LruCache;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

public class Hilogger extends AbstractPrinter {

    private static Hilogger instance;
    private final int DOMAIN = 9;

    private LruCache<String, HiLogLabel> lruCache = new LruCache<>(10);

    public static Hilogger getInstance(){
        if (instance == null) {
            synchronized (Hilogger.class) {
                if (instance == null) {
                    instance = new Hilogger();
                }
            }
        }

        return instance;
    }

    private Hilogger() {

    }

    @Override
    public void printLog(int type, String tag, String msg) {
        try {
            if (!LogConfigImpl.getInstance().isShowBorder()) {
                msg = getTopStackInfo() + ": " + msg;
            }

            HiLogLabel hiLogLabel = lruCache.get(tag);
            if (hiLogLabel == null) {
                lruCache.put(tag, new HiLogLabel(HiLog.LOG_APP, DOMAIN, tag));
                hiLogLabel = lruCache.get(tag);
            }

            switch (type) {
                case LogLevel.TYPE_DEBUG:
                    HiLog.debug(hiLogLabel, msg);
                    break;
                case LogLevel.TYPE_INFO:
                    HiLog.info(hiLogLabel, msg);
                    break;
                case LogLevel.TYPE_WARM:
                    HiLog.warn(hiLogLabel, msg);
                    break;
                case LogLevel.TYPE_ERROR:
                    HiLog.error(hiLogLabel, msg);
                    break;
                case LogLevel.TYPE_WTF:
                    HiLog.fatal(hiLogLabel, msg);
                    break;
                default:
                    HiLog.debug(hiLogLabel, msg);
                    break;
            }
        } catch (Exception ex) {
            HiLog.error(new HiLogLabel(HiLog.LOG_APP, DOMAIN, tag), HiLog.getStackTrace(ex));
        }
    }
}
