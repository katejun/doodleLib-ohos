package cn.talk.doodledemo.utils.log.interfaces;

public interface Printer {
    void d(String message, Object... args);
    void d(Object object);

    void e(String message, Object... args);
    void e(Object object);

    void w(String message, Object... args);
    void w(Object object);

    void i(String message, Object... args);
    void i(Object object);

    void v(String message, Object... args);
    void v(Object object);

    void json(String json);
}
