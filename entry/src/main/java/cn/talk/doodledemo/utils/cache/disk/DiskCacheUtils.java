package cn.talk.doodledemo.utils.cache.disk;

import cn.talk.doodledemo.utils.log.LogUtils;
import cn.talk.doodledemo.utils.photocodeclib.PhotoDecode;
import cn.talk.doodledemo.utils.photocodeclib.PhotoEncode;
import ohos.app.Context;
import ohos.media.image.PixelMap;

import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class DiskCacheUtils {
    private static final String TAG = "DiskCacheUtils";

    /**
     * image加入缓存
     */
    public static void putDataToDiskCache(String url, PixelMap bitmap,DiskLruCache diskLruCache) {

        try {
            String key = getMd5String(url);
            DiskLruCache.Editor editor = diskLruCache.edit(key);
            if (editor != null) {
                OutputStream outputStream = editor.newOutputStream(0);
                if (savePixelMap(bitmap, outputStream)) {
                    editor.commit();
                } else {
                    editor.abort();
                }
            }
            diskLruCache.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 保存 pixelMap
     *
     * @param pixelMap
     * @param outputStream
     * @return
     */
    private static boolean savePixelMap(PixelMap pixelMap, OutputStream outputStream) {
        return new PhotoEncode.Bulider().build().encodePhoto(outputStream, pixelMap);
    }

    //从缓存中取出 bitmap
    public static PixelMap getDataFromDiskCache(String url,DiskLruCache diskLruCache) {
        FileDescriptor fileDescriptor = null;
        FileInputStream fileInputStream = null;
        DiskLruCache.Snapshot snapshot = null;
        try {
            String key = getMd5String(url);
            LogUtils.d(TAG, "Disk Cache key" + key);

            snapshot = diskLruCache.get(key);
            if (snapshot != null) {
                fileInputStream = (FileInputStream) snapshot.getInputStream(0);
                PhotoDecode.Builder builder = new PhotoDecode.Builder();
                PixelMap pixelMap = builder.build().commonDecodePhoto(fileInputStream);
                return pixelMap;
            }else{
                return null;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fileDescriptor == null && fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    /**
     * 获取 version code
     *
     * @param context
     * @return
     */
    public static int getAppVersionCode(Context context) {
        //TODO
        return 1;
    }

    public static String getMd5String(String key) {
        String cacheKey;
        try {
            final MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.update(key.getBytes());
            cacheKey = bytesToHexString(messageDigest.digest());
        } catch (NoSuchAlgorithmException e) {
            cacheKey = String.valueOf(key.hashCode());
        }
        return cacheKey;
    }

    private static String bytesToHexString(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(0xFF & bytes[i]);
            if (hex.length() == 1) {
                sb.append('0');
            }
            sb.append(hex);
        }
        return sb.toString();
    }

}
