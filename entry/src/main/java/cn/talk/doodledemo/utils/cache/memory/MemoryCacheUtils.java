package cn.talk.doodledemo.utils.cache.memory;

import ohos.media.image.PixelMap;

public class MemoryCacheUtils {
    public static void put(String url, PixelMap bitmap,LruCache<String, PixelMap> pixelmapLruCache) {
        pixelmapLruCache.put(url, bitmap);
    }

    public static PixelMap get(String url,LruCache<String, PixelMap> pixelmapLruCache) {
        return pixelmapLruCache.get(url);
    }
}
