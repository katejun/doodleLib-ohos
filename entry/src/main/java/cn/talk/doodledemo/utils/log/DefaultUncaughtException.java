package cn.talk.doodledemo.utils.log;

import ohos.app.Context;

import java.util.Date;

public class DefaultUncaughtException implements Thread.UncaughtExceptionHandler {

    private Context mContext;

    public DefaultUncaughtException(Context context) {
        mContext = context;
    }

    @Override
    public void uncaughtException(Thread thread, Throwable throwable) {
        LogUtils.getLogConfig()
                .configAllowLog(true)
                .configLevel(LogLevel.TYPE_DEBUG);

        String tag = "crash";
        LogUtils.setTag(tag);
        String format = String.format("%s,%s,%s", thread.getName(), throwable.getStackTrace()[0],
                throwable.fillInStackTrace().toString());
        throwable.printStackTrace(System.out);
        LogUtils.e(format);

    }
}
