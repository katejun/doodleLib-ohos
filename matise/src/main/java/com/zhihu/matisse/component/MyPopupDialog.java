package com.zhihu.matisse.component;

import ohos.agp.components.Component;
import ohos.agp.window.dialog.PopupDialog;
import ohos.app.Context;

public class MyPopupDialog extends PopupDialog {
    public MyPopupDialog(Context context, Component contentComponent) {
        super(context, contentComponent);
    }

    public MyPopupDialog(Context context, Component contentComponent, int width, int height) {
        super(context, contentComponent, width, height);
    }

    @Override
    protected void onHide() {
        super.onHide();
        destroy();
    }

}
