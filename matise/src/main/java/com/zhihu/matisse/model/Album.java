/*
 * Copyright (C) 2014 nohana, Inc.
 * Copyright 2017 Zhihu Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an &quot;AS IS&quot; BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zhihu.matisse.model;



import com.zhihu.matisse.loader.AlbumLoader;
import ohos.app.Context;
import ohos.data.resultset.ResultSet;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;
import ohos.utils.Parcel;
import ohos.utils.Sequenceable;
import ohos.utils.net.Uri;

import java.io.IOException;

public class Album implements Sequenceable {

    public static final String ALBUM_ID_ALL = String.valueOf(-1);
    public static final String ALBUM_NAME_ALL = "All";

    private final String mId;
    private final Uri mCoverUri;
    private final String mDisplayName;
    private long mCount;

    public Album(String id, Uri coverUri, String albumName, long count) {
        mId = id;
        mCoverUri = coverUri;
        mDisplayName = albumName;
        mCount = count;
    }

    private Album(Parcel source) {
        mId = source.readString();
        mCoverUri = source.createSequenceable();
        mDisplayName = source.readString();
        mCount = source.readLong();
    }

    public static Album valueOf(ResultSet cursor) {
        String clumn = cursor.getString(cursor.getColumnIndexForName("uri"));
        return new Album(
                cursor.getString(cursor.getColumnIndexForName("_id")),
                clumn != null ? Uri.parse(clumn) : null,
                cursor.getString(cursor.getColumnIndexForName("_display_name")),
                cursor.getLong(cursor.getColumnIndexForName("count")));
    }



    public String getId() {
        return mId;
    }

    public Uri getCoverUri() {
        return mCoverUri;
    }

    public long getCount() {
        return mCount;
    }

    public void addCaptureCount() {
        mCount++;
    }

    public String getDisplayName(Context context) {
        if (isAll()) {
            try {
                return context.getResourceManager().getElement(com.zhihu.matisse.ResourceTable.String_album_name_all).getString();
            } catch (IOException | NotExistException | WrongTypeException e) {
                return "All Media";
            }
        }
        return mDisplayName;
    }

    @Override
    public String toString() {
        return "Album{" +
                "mId='" + mId + '\'' +
                ", mCoverUri=" + mCoverUri +
                ", mDisplayName='" + mDisplayName + '\'' +
                ", mCount=" + mCount +
                '}';
    }

    public boolean isAll() {
        return ALBUM_ID_ALL.equals(mId);
    }

    public boolean isEmpty() {
        return mCount == 0;
    }

    @Override
    public boolean hasFileDescriptor() {
        return false;
    }

    @Override
    public boolean marshalling(Parcel parcel) {
        parcel.writeString(mId);
        parcel.writeTypedSequenceable(mCoverUri);
        parcel.writeString(mDisplayName);
        return parcel.writeLong(mCount);
    }

    @Override
    public boolean unmarshalling(Parcel parcel) {
        return false;
    }

    public static final Sequenceable.Producer PRODUCER = Album::new;
}