/*
 * Copyright (C) 2014 nohana, Inc.
 * Copyright 2017 Zhihu Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an &quot;AS IS&quot; BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zhihu.matisse.model;


import com.zhihu.matisse.MimeType;
import ohos.aafwk.ability.DataUriUtils;
import ohos.data.resultset.ResultSet;
import ohos.media.photokit.metadata.AVStorage;
import ohos.utils.Parcel;
import ohos.utils.Sequenceable;
import ohos.utils.net.Uri;

public class Item implements Sequenceable{

    public static final long ITEM_ID_CAPTURE = -1;
    public static final String ITEM_DISPLAY_NAME_CAPTURE = "Capture";
    public long id;
    public String mimeType;
    public Uri uri;
    public long size;
    public String data;
    public long duration; // only for video, in ms

    private Item(long id, String mimeType, long size,String data, long duration) {
        this.id = id;
        this.mimeType = mimeType;
        Uri contentUri;
        if (isImage()) {
            contentUri = AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI;
        } else if (isVideo()) {
            contentUri = AVStorage.Video.Media.EXTERNAL_DATA_ABILITY_URI;
        } else {
            // ?
            contentUri = AVStorage.Files.fetchResource("external");
        }
        this.uri = DataUriUtils.attachId(contentUri, id);
        this.size = size;
        this.data = data;
        this.duration = duration;
    }

    public static Item valueOf(ResultSet cursor) {
        long duration = cursor.getLong(cursor.getColumnIndexForName(AVStorage.AVBaseColumns.DURATION));
        return new Item(cursor.getLong(cursor.getColumnIndexForName(AVStorage.AVBaseColumns.ID)),
                cursor.getString(cursor.getColumnIndexForName(AVStorage.AVBaseColumns.MIME_TYPE)),
                cursor.getLong(cursor.getColumnIndexForName(AVStorage.AVBaseColumns.SIZE)),
                cursor.getString(cursor.getColumnIndexForName(AVStorage.AVBaseColumns.DATA)),
                duration);
    }


    public Uri getContentUri() {
        return uri;
    }

    public boolean isCapture() {
        return id == ITEM_ID_CAPTURE;
    }

    public boolean isImage() {
        return MimeType.isImage(mimeType);
    }

    public boolean isGif() {
        return MimeType.isGif(mimeType);
    }

    public boolean isVideo() {
        return MimeType.isVideo(mimeType);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Item)) {
            return false;
        }

        Item other = (Item) obj;
        return id == other.id
                && (mimeType != null && mimeType.equals(other.mimeType)
                    || (mimeType == null && other.mimeType == null))
                && (uri != null && uri.equals(other.uri)
                    || (uri == null && other.uri == null))
                && size == other.size
                && duration == other.duration;
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = 31 * result + Long.valueOf(id).hashCode();
        if (mimeType != null) {
            result = 31 * result + mimeType.hashCode();
        }
        result = 31 * result + uri.hashCode();
        result = 31 * result + Long.valueOf(size).hashCode();
        result = 31 * result + Long.valueOf(duration).hashCode();
        return result;
    }

    @Override
    public boolean hasFileDescriptor() {
        return false;
    }

    @Override
    public boolean marshalling(Parcel parcel) {
        if (!parcel.writeLong(id)){
            return false;
        }
        if (!parcel.writeString(mimeType)){
            return false;
        }
        parcel.writeTypedSequenceable(uri);

        if (!parcel.writeLong(size)){
            return false;
        }
        if (!parcel.writeString(data)){
            return false;
        }
        return parcel.writeLong(duration);
    }

    @Override
    public String toString() {
        return "Item{" +
                "id=" + id +
                ", mimeType='" + mimeType + '\'' +
                ", uri=" + uri +
                ", size=" + size +
                ", data='" + data + '\'' +
                ", duration=" + duration +
                '}';
    }

    @Override
    public boolean unmarshalling(Parcel parcel) {
        id = parcel.readLong();
        mimeType = parcel.readString();
        uri = parcel.createSequenceable();
        size = parcel.readLong();
        data = parcel.readString();
        duration = parcel.readLong();
        return true;
    }
    public Item(Parcel parcel){
        id = parcel.readLong();
        mimeType = parcel.readString();
        uri = parcel.createSequenceable();
        size = parcel.readLong();
        data = parcel.readString();
        duration = parcel.readLong();
    }

    public static final Sequenceable.Producer PRODUCER = new Sequenceable.Producer(){
        @Override
        public Object createFromParcel(Parcel parcel) {
            return new Item(parcel);
        }
    };
}
