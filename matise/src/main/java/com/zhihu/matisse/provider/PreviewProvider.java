package com.zhihu.matisse.provider;

import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.ResourceTable;
import com.zhihu.matisse.component.CheckView;
import com.zhihu.matisse.model.Item;
import com.zhihu.matisse.utils.L;
import com.zhihu.matisse.utils.TimeUtils;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.DataAbilityRemoteException;
import ohos.agp.components.*;
import ohos.app.Context;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.photokit.metadata.AVMetadataHelper;
import ohos.utils.net.Uri;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class PreviewProvider extends PageSliderProvider {
    private ArrayList<Item> list;
    private Context mContext;

    private OnPrimaryItemSetListener onPrimaryItemSetListener;

    public void setOnPrimaryItemSetListener(OnPrimaryItemSetListener onPrimaryItemSetListener) {
        this.onPrimaryItemSetListener = onPrimaryItemSetListener;
    }

    public PreviewProvider(Context context, ArrayList<Item> list){
        this.list = list;
        mContext = context;
    }


    public Item getMediaItem(int position) {
        return list.get(position);
    }

    public void addAll(List<Item> items) {
        list.addAll(items);
    }

    @Override
    public int getCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public Object createPageInContainer(ComponentContainer componentContainer, int i) {
        Component v = LayoutScatter.getInstance(mContext).parse(com.zhihu.matisse.ResourceTable.Layout_preview_item, componentContainer, false);
        Image image = (Image) v.findComponentById(ResourceTable.Id_imagePreview_item);
        Image play = (Image) v.findComponentById(ResourceTable.Id_imagePreview_play);
        Item item = list.get(i);
        if (MimeType.isImage(item.mimeType)) {
            play.setVisibility(Component.HIDE);
//            Uri uri = Uri.appendEncodedPathToUri(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI, String.valueOf(item.id));
            ImageSource.SourceOptions options = new ImageSource.SourceOptions();
            try {
                ImageSource imageSource = ImageSource.create(DataAbilityHelper.creator(mContext).openFile(item.uri, "r"), options);
                ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
                decodingOptions.editable = true;

                PixelMap pixelMap = imageSource.createPixelmap(decodingOptions);
                image.setPixelMap(pixelMap);
            } catch (DataAbilityRemoteException | FileNotFoundException e) {
                L.d(e.getMessage());
            }
        } else if (MimeType.isVideo(item.mimeType)) {
            play.setVisibility(Component.VISIBLE);
            AVMetadataHelper avMetadataHelper = new AVMetadataHelper();
            try {
                if (avMetadataHelper.setSource(DataAbilityHelper.creator(mContext).openFile(item.uri, "r"))){
                    image.setPixelMap(avMetadataHelper.fetchVideoPixelMapByTime(1));
                }
            } catch (DataAbilityRemoteException | FileNotFoundException e) {
                L.d(e.getMessage());
            }
            avMetadataHelper.release();
        }
        play.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (onPrimaryItemSetListener != null){
                    onPrimaryItemSetListener.onPrimaryItemSet(i,item.uri );
                }
            }
        });

        componentContainer.addComponent(v);
        return v;
    }

    @Override
    public void destroyPageFromContainer(ComponentContainer componentContainer, int i, Object o) {
        Component component = (Component) o;
        componentContainer.removeComponent(component);
    }

    @Override
    public boolean isPageMatchToObject(Component component, Object o) {
        return component == o;
    }

    public interface OnPrimaryItemSetListener {

        void onPrimaryItemSet(int position,Uri uri);
    }
}
