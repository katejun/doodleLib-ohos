package com.zhihu.matisse.provider;

import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.ResourceTable;
import com.zhihu.matisse.component.CheckView;
import com.zhihu.matisse.model.*;
import com.zhihu.matisse.utils.L;
import com.zhihu.matisse.utils.TimeUtils;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.DataAbilityRemoteException;
import ohos.agp.components.*;
import ohos.app.Context;
import ohos.media.photokit.metadata.AVMetadataHelper;
import ohos.media.photokit.metadata.AVStorage;
import ohos.utils.net.Uri;

import java.io.FileNotFoundException;
import java.util.ArrayList;

public class MediaProvider extends BaseItemProvider {

    private ArrayList<Item> list;
    private Context mContext;
    private SelectedItemCollection mCollection;
    private SelectionSpec selectionSpec;
    private CheckStateListener mCheckStateListener;
    private OnMediaClickListener mOnMediaClickListener;

    private int spanCount = 1;

    public MediaProvider(Context context, ArrayList<Item> list, SelectedItemCollection collection,int spanCount) {
        selectionSpec = SelectionSpec.getInstance();
        this.list = list;
        mContext = context;
        mCollection = collection;
        this.spanCount = spanCount;
    }

    @Override
    public int getCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return list == null ? 0 : list.get(i).hashCode();
    }

    public ArrayList<Item> getList() {
        return list;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        Component v = LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_media_grid_layout, componentContainer, false);

        int screenWidth = AttrHelper.fp2px(mContext.getResourceManager().getDeviceCapability().width,mContext);
        int w = screenWidth / spanCount;
        ComponentContainer.LayoutConfig layoutConfig = new ComponentContainer.LayoutConfig(w,w);
        v.setLayoutConfig(layoutConfig);

        Image image = (Image) v.findComponentById(ResourceTable.Id_media_thumbnail);
        Text duration = (Text) v.findComponentById(ResourceTable.Id_media_duration);
        Image gif = (Image) v.findComponentById(ResourceTable.Id_media_gif);
        CheckView checkView = (CheckView) v.findComponentById(ResourceTable.Id_media_check);
        Text tvCamera = (Text) v.findComponentById(ResourceTable.Id_media_camera);
        Item item= (Item) getItem(i);

        if (item.id < 0){
            image.setVisibility(Component.HIDE);
            checkView.setVisibility(Component.HIDE);
            duration.setVisibility(Component.HIDE);
            gif.setVisibility(Component.HIDE);
            tvCamera.setVisibility(Component.VISIBLE);


        }else {
            image.setVisibility(Component.VISIBLE);
            checkView.setVisibility(Component.VISIBLE);
            tvCamera.setVisibility(Component.HIDE);
            checkView.setCountable(selectionSpec.countable);

            if (MimeType.isImage(item.mimeType)) {
                Uri uri = Uri.appendEncodedPathToUri(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI, String.valueOf(item.id));
                selectionSpec.imageEngine.loadImage(mContext,0,0,image,uri);
                if (MimeType.isGif(item.mimeType)) {
                    gif.setVisibility(Component.VISIBLE);
                } else {
                    gif.setVisibility(Component.HIDE);
                }
                duration.setVisibility(Component.HIDE);
            } else if (MimeType.isVideo(item.mimeType)) {
                gif.setVisibility(Component.HIDE);
                duration.setVisibility(Component.VISIBLE);
                Uri uri = Uri.appendEncodedPathToUri(AVStorage.Video.Media.EXTERNAL_DATA_ABILITY_URI, String.valueOf(item.id));
                AVMetadataHelper avMetadataHelper = new AVMetadataHelper();
                duration.setText(TimeUtils.formatElapsedTime(item.duration / 1000));
                try {
                    if (avMetadataHelper.setSource(DataAbilityHelper.creator(mContext).openFile(uri, "r"))){
                        image.setPixelMap(avMetadataHelper.fetchVideoPixelMapByTime(1));
                    }
                } catch (DataAbilityRemoteException | FileNotFoundException e) {
                    L.d(e.getMessage());
                }
                avMetadataHelper.release();
            }
            checkView.setClickedListener(component12 -> updateSelectedItem(item,mContext));
            setCheckStatus(item,checkView);
        }
        v.setClickedListener(component1 -> {
            if (item.isCapture()){
                if (mOnMediaClickListener != null) {
                    mOnMediaClickListener.onMediaSelect(null, item, i);
                }
            }else {
                if (selectionSpec.showPreview){
                    if (mOnMediaClickListener != null) {
                        mOnMediaClickListener.onMediaSelect(null, item, i);
                    }
                }else {
                    updateSelectedItem(item,mContext);
                }
            }

        });
        return v;
    }

    private void setCheckStatus(Item item,CheckView checkView){
        if (selectionSpec.countable){
            int checkNum = mCollection.checkedNumOf(item);
            if (checkNum > 0){
                checkView.setEnabled(true);
                checkView.setCheckedNum(checkNum);
            }else {
                if (mCollection.maxSelectableReached()){
                    checkView.setEnabled(false);
                    checkView.setCheckedNum(CheckView.UNCHECKED);
                }else {
                    checkView.setEnabled(true);
                    checkView.setCheckedNum(checkNum);
                }
            }
        }else {
            boolean selected = mCollection.isSelected(item);
            if (selected) {
                checkView.setEnabled(true);
                checkView.setChecked(true);
            } else {
                if (mCollection.maxSelectableReached()) {
                    checkView.setEnabled(false);
                    checkView.setChecked(false);
                } else {
                    checkView.setEnabled(true);
                    checkView.setChecked(false);
                }
            }
        }
    }
    private void updateSelectedItem(Item item,Context holder) {
        if (selectionSpec.countable) {
            int checkedNum = mCollection.checkedNumOf(item);
            if (checkedNum == CheckView.UNCHECKED) {
                if (assertAddSelection(holder, item)) {
                    mCollection.add(item);
                    notifyCheckStateChanged();
                }
            } else {
                mCollection.remove(item);
                notifyCheckStateChanged();
            }
        } else {
            if (mCollection.isSelected(item)) {
                mCollection.remove(item);
                notifyCheckStateChanged();
            } else {
                if (assertAddSelection(holder, item)) {
                    mCollection.add(item);
                    notifyCheckStateChanged();
                }
            }
        }
    }
    public void registerOnMediaClickListener(OnMediaClickListener listener) {
        mOnMediaClickListener = listener;
    }

    public void unregisterOnMediaClickListener() {
        mOnMediaClickListener = null;
    }
    public void registerCheckStateListener(CheckStateListener listener) {
        mCheckStateListener = listener;
    }

    public void unregisterCheckStateListener() {
        mCheckStateListener = null;
    }
    private void notifyCheckStateChanged() {
        notifyDataChanged();
        if (mCheckStateListener != null) {
            mCheckStateListener.onUpdate();
        }
    }
    private boolean assertAddSelection(Context context, Item item) {
        IncapableCause cause = mCollection.isAcceptable(item);
        IncapableCause.handleCause(context, cause);
        return cause == null;
    }
    public interface CheckStateListener {
        void onUpdate();
    }
    public interface OnMediaClickListener {
        void onMediaSelect(Album album, Item item, int adapterPosition);
    }
}
