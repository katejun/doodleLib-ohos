package com.zhihu.matisse.slice;

import com.zhihu.matisse.ResourceTable;
import com.zhihu.matisse.utils.L;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Image;
import ohos.agp.components.surfaceprovider.SurfaceProvider;
import ohos.agp.graphics.SurfaceOps;
import ohos.media.common.Source;
import ohos.media.player.Player;
import ohos.utils.net.Uri;

import java.io.FileDescriptor;
import java.util.function.Consumer;

public class VideoAbility extends Ability {
    private static final String TAG = "VideoAbility";
    private SurfaceProvider surfaceView;

    private Player player;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_video);

        surfaceView = (SurfaceProvider) findComponentById(ResourceTable.Id_surface);

        Uri playUri = intent.getUri();

        player = new Player(getContext());
        Source source;
        try {
            FileDescriptor r = DataAbilityHelper.creator(this).openFile(playUri, "r");
            source = new Source(r);
            player.setSource(source);
            player.setPlayerCallback(mplayerCallback);
            player.prepare();
            addSurfaceView();

        } catch (Exception e) {
            L.d(e.getMessage());
        }
        Image image = (Image) findComponentById(ResourceTable.Id_video_back);
        image.setClickedListener(component -> terminateAbility());
    }


    private void addSurfaceView() {
        surfaceView.pinToZTop(true);
        surfaceView.getSurfaceOps().ifPresent(new Consumer<SurfaceOps>() {
            @Override
            public void accept(SurfaceOps surfaceOps) {
                surfaceOps.addCallback(new SurfaceOps.Callback() {
                    @Override
                    public void surfaceCreated(SurfaceOps surfaceOps) {
                        player.setSurfaceOps(surfaceOps);
                        player.play();

                    }

                    @Override
                    public void surfaceChanged(SurfaceOps surfaceOps, int i, int i1, int i2) {
                        L.info(TAG, "surfaceChanged() called.");
                    }

                    @Override
                    public void surfaceDestroyed(SurfaceOps surfaceOps) {
                        L.info(TAG, "surfaceDestroyed() called.");
                    }
                });
            }
        });

    }
    private Player.IPlayerCallback mplayerCallback = new Player.IPlayerCallback() {
        @Override
        public void onPrepared() {
        }

        @Override
        public void onMessage(int i, int i1) {
        }

        @Override
        public void onError(int i, int i1) {
        }

        @Override
        public void onResolutionChanged(int i, int i1) {
        }

        @Override
        public void onPlayBackComplete() {
            if (player != null) {
                player.stop();
                player = null;
            }
        }

        @Override
        public void onRewindToComplete() {

        }

        @Override
        public void onBufferingChange(int i) {
        }

        @Override
        public void onNewTimedMetaData(Player.MediaTimedMetaData mediaTimedMetaData) {
        }

        @Override
        public void onMediaTimeIncontinuity(Player.MediaTimeInfo mediaTimeInfo) {
        }
    };
}
