package com.zhihu.matisse.utils;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

public class L {

    private static HiLogLabel hiLogLabel = new HiLogLabel(HiLog.LOG_APP,0x2010,"LogUtil");

    public static void d(String message){
        HiLog.debug(hiLogLabel,message);
    }

    public static void d(String tag,String message){
        HiLog.debug(hiLogLabel,"TAG: %{public}s ; Message: %{public}s",tag,message);
    }
    public static void e(String tag,String message){
        HiLog.error(hiLogLabel,"TAG: %{public}s ; Message: %{public}s",tag,message);

    }

    public static void info(String tag,String message){
        HiLog.info(hiLogLabel,"TAG %{public}s ; Msg %{public}s",tag,message);
    }
}
